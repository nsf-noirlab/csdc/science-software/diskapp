Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

Unreleased
------------------

v0.11 (25/07/2024)
--------------------

Added
~~~~~
- now uses a toml config file

Changed
~~~~~~~

- refactoring code structure
- now installs diskapp! (placeholder)

Fixed
~~~~~
- vstruct - correctly resolve CWD on windows
- diskapp - remove species line when deselected
- both - fix reactions.py import bug


v0.10.2 (20/05/2024)
--------------------

Changed
~~~~~~~
- linting CI job
- test CI job
- specify valid radii
- zmin, zmax, t_0, t_max, n_t, imax, and n_thresh are not settable via command line

v0.10.1 (16/05/2024)
--------------------

Added
~~~~~
- Now installable with pipx

Changed
~~~~~~~
- using click instead of argparse
