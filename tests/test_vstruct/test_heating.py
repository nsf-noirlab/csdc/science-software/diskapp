from vstruct import heating


def test_alpha_h_constant():
    assert isinstance(heating.ALPHA_H, float)
    assert heating.ALPHA_H == 0.50
