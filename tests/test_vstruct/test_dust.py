from vstruct.dust import set_dust_properties


def test_set_properties():
    test_a_g = 1
    test_Q_abs = 2
    test_Q_ext = 3
    test_rho_bar = 4
    test_dust_gas_ratio = 5
    test_Y = 6
    test_E_phel = 7
    result = set_dust_properties(a_g=test_a_g,
                                 Q_abs=test_Q_abs,
                                 Q_ext=test_Q_ext,
                                 rho_bar=test_rho_bar,
                                 dust_gas_ratio=test_dust_gas_ratio,
                                 Y=test_Y,
                                 E_phel=test_E_phel)
    assert result["a_g"] == test_a_g
    assert result["Q_abs"] == test_Q_abs
    assert result["Q_ext"] == test_Q_ext
    assert result["rho_bar"] == test_rho_bar
    assert result["dust_gas_ratio"] == test_dust_gas_ratio
    assert result["Y"] == test_Y
    assert result["E_phel"] == test_E_phel
    assert isinstance(result["S_d"], float)  # calculation
    assert isinstance(result["sigma"], float)  # calculation
    assert isinstance(result["properties"], str)

