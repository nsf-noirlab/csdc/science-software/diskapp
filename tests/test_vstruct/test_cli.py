from unittest.mock import patch

from click.testing import CliRunner
from vstruct.vstruct import cli


@patch("vstruct.vstruct.vstruct", autospec=True)
def test_default_config(mock_vstruct):
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0
    mock_vstruct.assert_called_once()
    config = mock_vstruct.call_args[0][0]
    assert isinstance(config, dict)
    assert config["radius"] == 0.95
    assert config['LyaPerFUVContinuum'] == 3


@patch("vstruct.vstruct.vstruct", autospec=True)
def test_config_file_updates_params(mock_vstruct):
    runner = CliRunner()
    config_path = "tests/test_vstruct/valid_test_config.toml"
    result = runner.invoke(cli, ["--config-file", config_path])
    assert result.exit_code == 0
    mock_vstruct.assert_called_once()
    config = mock_vstruct.call_args[0][0]
    assert isinstance(config, dict)
    assert config["radius"] == 0.24


# TODO: Test a malformed config file
