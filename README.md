# vstruct
# diskapp (DIsk Structure, Kinetics Analysis, & Plotting Program)

:warning: This project is a **work in progress** :warning:

## Requirements

- Make

For local development:
- Python 3.12
- Poetry

For Dockerized use or dev:
- Docker

## QuickStart

- `poetry install`
- `poetry run vstruct {RADIUS}`

where RADIUS is a float of AU

## Examples

## Credits

Scientific contributions: Joan Najita
Originally written by Máté Ádámkovics @ Berekeley circa 2014
Reburbished by Destry Saul @ NOIRLab - 2024