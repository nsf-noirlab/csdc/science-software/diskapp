#!/usr/bin/env python

#! -------------------------------------------------------------------
#
# diskapp
#
# DIsk Structure, Kinetics Analysis, & Plotting Program (diskapp)
# 
# Application to analyze the output from models of the X-ray driven
# thermal chemistry in protoplanetary disks. The code used to 
# generate data diles are in the uv_calculation.ipynb notebooks in
#
#     ~/disk/chem/src/UV
#
#                                                mate@berkeley.edu
#! -------------------------------------------------------------------


import time
import re
import zipfile
import zipimport
from io import StringIO, BytesIO

import wx
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
from matplotlib.figure import Figure
from matplotlib.backends.backend_wx import NavigationToolbar2Wx

from numpy import exp

from traits.api import HasTraits, Instance, Str, \
           Button, Constant, Bool, Int, List, Font, \
           Float, String, Color

from traitsui.api \
    import View, Item, Group, HSplit, Handler, TextEditor, \
           CheckListEditor, TabularEditor, RangeEditor

from traitsui.wx.editor import Editor
from traitsui.basic_editor_factory import BasicEditorFactory
from traitsui.tabular_adapter import TabularAdapter
from pyface.api import FileDialog, OK
from pylab import setp

from diskapp import reactions


yr = 60*60*24*365.25 # seconds per year
eps = 1e-40 # lower limit for thermal rate figure

class _MPLFigureEditor(Editor):
    scrollable  = True
    def init(self, parent):
        self.control = self._create_canvas(parent)
        self.set_tooltip()
        
    def update_editor(self):
        pass

    def _create_canvas(self, parent):
        """ Create the MPL canvas. """
        panel = wx.Panel(parent, -1, style=wx.CLIP_CHILDREN)
        sizer = wx.BoxSizer(wx.VERTICAL)
        panel.SetSizer(sizer)
        mpl_control = FigureCanvasWxAgg(panel, -1, self.value)
        sizer.Add(mpl_control, 1, wx.LEFT | wx.TOP | wx.GROW)
        toolbar = NavigationToolbar2Wx(mpl_control)
        sizer.Add(toolbar, 0, wx.EXPAND)
        self.value.canvas.SetMinSize((10,10))
        return panel

class MPLFigureEditor(BasicEditorFactory):
    klass = _MPLFigureEditor

class ReactionAdapter(TabularAdapter):
    columns           = [ ('#',   'number'),
                          ('Reaction', 'reaction'),
                          ('k',        'k'),
                          ('R',        'R') ]
    even_bg_color     = Color( ( 201, 223, 241 ) )
    font              = Font('Courier 13')
    number_width      = Float(0.12)
    reaction_width    = Float(0.48)
    k_width           = Float(0.20)
    R_width           = Float(0.20)

tabular_editor = TabularEditor( editable   = False,
                                selected   = 'table_reaction',
                                adapter    = ReactionAdapter(),
                                operations = [])

class ReactionLine (HasTraits):
    number    = Str
    reaction  = Str
    k         = Str
    R         = Str
    densities = Str
    formula   = Str

class ControlPanel(HasTraits):

    figure          = Instance(Figure)
    thermfig        = Instance(Figure)
    load_button     = Button("Load", width=0.5)
    file_wildcard   = Str("Numpy Data file (*.npy)|*.npy|All files|*")
    dataloaded      = False

    species_str     = Str 
    species         = List
    plot_species    = []

    therm_proc_str  = Str 
    therm_proc      = List
    plot_proc       = []

    nHlow, nHhigh   = Float(18.0), Float(24.0)
    vertical_column = Float

    tclow, tchigh   = Float(-7.0), Float(7.0)
    chem_time       = Float
    
    filename        = String("", label="File",
                             desc="output file from disk_vertical_struct.py")
    logfile         = String("", label="Log")
    log             = String("", label="Log",
                             desc="file log corresponding to datafile")

    rxn_selected    = List( ReactionLine )
                                   
    rxn_line        = Int()
    line            = Int()

    kinetics_button = Button("Reaction Info", width=0.20)
    kinetics        = String("", desc="Details for selected reaction.")
    
    get_num_data_button   = Button("Get Plot Data", width=0.2)
    numerical_data = String("", label="Plot Data",
                            desc="Numerical values from plot.")

    get_warm_column_button = Button("Get Warm Columns", width=0.2)
    T_min = Float(250.)
    T_max = Float(1000.)
    T_freeze = Float(125.)
    warm_column_data = String("", label="Warm Columns Data",
                            desc="Warm columns for selected species.")

    table_reaction  = ReactionLine

    def _thermfig_default(self):        
        figure = Figure()
        figure.subplots_adjust(left=0.10, bottom=0.07,right=0.95,
                               top=0.95, wspace=0.24, hspace=0)
        ax   = figure.add_subplot(111, position=[0.15,0.10,0.80,0.75])
        
        return figure

    view = View(Group(
        # DATA FILE TAB
                   Group( Item( "load_button",
                                label="Load Data",
                                show_label=False),
                          Item( "filename",
                                style="readonly"),
                          Item( "logfile",
                                style="readonly"),
                          Item( "log", style="readonly",
                                show_label = False,
                                height=1200,
                                editor=TextEditor()),
                          label = "Data File", dock = "tab"
                          ),
        # PLOT SPECIES TAB
                   Group( Item( "species_str",
                                show_label=False,
                                style="custom",
                                editor=CheckListEditor(
                                           name='species',
                                           format_func=lambda x:x,
                                           cols=6 )),
                          label = "Plot Species", dock='tab'
                          ),
        # KINETICS TAB                          
                   Group( Item( "vertical_column",
                                label="log N_H",
                                editor = RangeEditor(
                                            low_name = 'nHlow',
                                            high_name = 'nHhigh',
                                            #format= "%.1f",
                                            label_width = 50)
                                ),
                          Item( "chem_time",
                                label="timestep, log [yr]",
                                editor = RangeEditor(
                                            low_name = 'tclow',
                                            high_name = 'tchigh',
                                            #format= "%.1f",
                                            label_width = 50)
                                ),
                          Item( 'rxn_selected',
                                show_label = False,                               
                                editor = tabular_editor),
                          Item( "kinetics_button",
                                label="Reaction Info",
                                show_label=False),
                          Item( "kinetics", style="readonly",
                                show_label = False,
                                height=250,
                                editor=TextEditor()),
                          label = "Kinetics", dock='tab'        
                          ),
        # THERMAL RATES PLOT
                    Group(Item( "therm_proc_str",
                                show_label=False,
                                style="custom",
                                editor=CheckListEditor(
                                           name='therm_proc',
                                           format_func=lambda x:x,
                                           cols=6, 
                                           )
                                ),
                          Item( "thermfig",
                                 show_label=False,
                                 editor=MPLFigureEditor()
                                 ),
                           label = "Thermal Rates", dock='tab'
                          ),
        # NUMERICAL DATA FROM PLOT
                    Group(Item( "get_num_data_button",
                                label="Get Plot Data",
                                show_label=False),
                          Item( "numerical_data", style="readonly",
                                show_label = False,
                                height=1200,
                                editor=TextEditor() ),
                          label = "Plot Numbers", dock='tab'
                          ),
        # WARM COLUMNS
                    Group(Item( "T_min",    ),
                          Item( "T_max",    ),
                          Item( "T_freeze", ),
                          Item( "get_warm_column_button",
                                label="Get Warm Columns",
                                show_label=False,
                                width=400),
                          Item( "warm_column_data", 
                                style="readonly",
                                show_label = False,
                                height=300,
                                editor=TextEditor() ),
                          label = "Warm Columns", dock='tab'
                          ),
                   layout='tabbed')
               )

    def _vertical_column_changed(self):
        """ Update kinetics plot for change vertical column """
        if self.dataloaded and len(self.plot_species): 
            self.update_kinetics()
               
    def _chem_time_changed(self):
        if self.dataloaded and len(self.plot_species): 
            z = abs(self.data['N_H'] - 10**self.vertical_column).argmin()
            t = abs(self.data['t'] - (10**self.chem_time)*yr).argmin()
            ax = self.figure.axes[0]
            if len(self.plot_species):
                for (i,x) in enumerate(self.plot_species) :
                    if x in ['T','T_d','flare']:
                        ax.lines[i].set_data(np.log10(self.data['N_H'][1:]),
                            np.log10(self.data['n'][x][1:,t]) )
                    else:
                        ax.lines[i].set_data(
                            np.log10(self.data['N_H'][1:]), 
                            np.log10(self.data['n'][x][1:,t]/self.data['nH'][1:]))
                self.update_kinetics()
            self.figure.canvas.draw()

    def _species_str_changed(self):
        """ Update plot and reactions each time species list is changed."""

        z = abs(self.data['N_H'] - 10**self.vertical_column).argmin()
        t = abs(self.data['t'] - (10**self.chem_time)*yr).argmin()

        lw = 1.5
        selected_species = self.species_str[1:].split(',')

        ax  = self.figure.axes[0]

        if len(selected_species) > len(self.plot_species):
            x = selected_species[-1]
            self.plot_species.append(x)
            ax.plot(np.log10(self.data['N_H'][1:]), 
                    np.log10(self.data['n'][x][1:,t]/self.data['nH'][1:]),
                    lw=lw, label=x, drawstyle='steps-mid' )                    
            ax.autoscale(enable=True, axis='y')
            
        else:
            ind = [i for (i, x) in enumerate(self.plot_species)
                   if x not in selected_species][0]
            if ind > -1:
                self.plot_species.pop(ind)
                ax.lines[ind].remove()
        
        if len(self.plot_species):
            ax.legend(loc='best', prop={"size":12})
        else:
            ax.legend_ = None

        self.figure.canvas.draw()
        self.update_kinetics()

    def _therm_proc_str_changed(self):
        """ Update thermal rates figure"""

        z = abs(self.data['N_H'] - 10**self.vertical_column).argmin()
        t = abs(self.data['t'] - (10**self.chem_time)*yr).argmin()

        lw = 1.5
        selected_proc = self.therm_proc_str[1:].split(',')

        ax  = self.thermfig.axes[0]

        if len(selected_proc) > len(self.plot_proc):
            x = selected_proc[-1]
            self.plot_proc.append(x)
            ax.plot(np.log10(self.data['N_H']), 
                    np.log10(self.data['therm'][x]),
                    lw=lw, label=x, drawstyle='steps-mid' )                    
            ax.autoscale(enable=True, axis='y')
            
        else:
            ind = [i for (i,x) in enumerate(self.plot_proc)
                   if x not in selected_proc][0]
            if ind > -1:
                self.plot_proc.pop(ind)
                ax.lines.pop(ind)
        
        if len(self.plot_proc):
            ax.legend(loc='best', prop={"size":12})
        else:
            ax.legend_ = None

        self.thermfig.canvas.draw()


    def update_kinetics(self):
        """Get reactions for selected species and sort by reaction rate."""

        z = abs(self.data['N_H'] - 10**self.vertical_column).argmin()
        t = abs(self.data['t'] - (10**self.chem_time)*yr).argmin()

        n    = self.data['n'][z,t]
        zeta = self.data['zeta'][z]

        all_reactions = reactions.rates(n, zeta, self.data['dust']['a_g'], 
                                        self.data['F_FUV'][z],
                                        radius=self.radius,
                                        G_FUV=self.data['G_FUV'][z])

        selected_reactions = [] ; selected_r_numbers =[] 
        
        for x in self.plot_species :
            for reaction in all_reactions:
                if ' '+x+' ' in reaction[4]+' ' or '*'+x+' ' in reaction[4]+' ' :
                    if not reaction[0] in selected_r_numbers:
                        selected_r_numbers.append(reaction[0])
                        selected_reactions.append(reaction)

        output = [ ReactionLine(number="{:6s}".format(reaction[0]).upper(), 
                                reaction="{:25s}".format(reaction[4]),
                                k="{:9.2e}".format(reaction[3]),
                                R="{:9.2e}".format(reaction[2]),
                                densities="".join(["({:5.1e})  ".format(
                                    n) for n in reaction[1]]),
                                formula=reaction[5] )
                   for reaction in sorted(selected_reactions,
                                          key=lambda reaction: reaction[2],
                                          reverse=True) ]

        self.rxn_selected = output


    def _kinetics_button_fired(self):
        z = abs(self.data['N_H'] - 10**self.vertical_column).argmin()
        t = abs(self.data['t'] - (10**self.chem_time)*yr).argmin()
        n = self.data['n'][z,t]
        T_gas = n['T']
        
        nH = self.data['nH'][z]
        NH = self.data['N_H'][z]
                      
        self.kinetics = """
     {:s} : {:s}
            {:s}

      T_gas: {:5.1f}K,   N_H:{:6.1e} cm-2,  n_H:{:6.1e}[cm-3]

      Rate coefficient formula:
         R ={:s}
         k ={:s}
            """.format(self.table_reaction.number,
                       self.table_reaction.reaction,
                       self.table_reaction.densities, 
                       T_gas, NH, nH,  
                       self.table_reaction.formula.replace(
                           'p','+').replace('m','-').replace('ex+','exp').strip(),
                       self.table_reaction.k )

    def dataset_show(self) :
        """ Update plots with newly loaded dataset."""

        lw = 1.5
        a  = 0.85
        ds = 'steps-mid'

        self.vertical_column = self.nHhigh
        z = abs(self.data['N_H'] - 10**self.vertical_column).argmin()
        t = abs(self.data['t'] - 10**self.chem_time*yr).argmin()
        x  = np.log10(self.data['N_H'][1:])
        self.suptitle = self.figure.suptitle(
                             self.filename + 13*" " + 
                             "radius ={:5.2f}AU".format(self.radius) + 
                             13*" " + time.strftime('%B %d, %Y'))
        ax1 = self.figure.axes[1]
        ax1.set_xlim( (self.nHlow, self.nHhigh) )
        ax1.plot(x, self.data['n']['T'][1:,-1], 'b-', lw=lw, label='T_gas', drawstyle=ds)
        ax1.plot(x, self.data['n']['T_d'][1:,-1], 'k-', label='T_dust',drawstyle=ds )
        ax1.legend(loc="best", prop={"size":12})
        ax2 = self.thermfig.axes[0]
        ax2.cla()
        fs = 14
        ax2.set_ylabel('thermal rates [erg/cm3/s]', fontsize=fs)
        ax2.set_xlabel('vertical column density, log($N_H$)', fontsize=fs)
        ax2.set_xlim(19,23)
        ax2.set_ylim((-15.4,-7.5))
        ax2.grid(which='both')
        self.figure.canvas.draw()

    def _get_num_data_button_fired(self):
        axT = self.figure.axes[1]
        nl = len(axT.get_lines())
        N_H = axT.get_lines()[0].get_xdata()
        odata = ( N_H, axT.get_lines()[0].get_ydata(),
                  axT.get_lines()[1].get_ydata(), )
        ax = self.figure.axes[0]
        for line in  ax.get_lines(): odata = odata + (line.get_ydata(),)
        ostr=""
        for x in range(len(odata[0])) : 
            for item in odata :
                ostr = ostr + "{:^12.2f} &".format(item[x])
            ostr += "\\\\\n"
        self.numerical_data = ostr

    def _get_warm_column_button_fired(self):
        """Return the warm column of a species in a dataset, exclude
        regions of freeze-out onto grains at temperature, T_freeze, 
        or calculate the critical density using T_dust."""
 
        dataset = self.data
        dz = dataset['dz']
 
        selected_species = self.species_str[1:].split(',')
        T_min=self.T_min
        T_max=self.T_max
        T_freeze=self.T_freeze

        T = dataset['n']['T'][:,-1]
        T_d = dataset['n']['T_d'][:,-1]
        nH = dataset['nH']
        N_H = dataset['N_H']

        warm = np.where([(T>T_min) & (T<T_max) & (T_d>T_freeze) & (N_H<1e23)])[1]

        ostr = """\
        Warm columns calculations using:
         T_min = {T_min:7.1f} K, T_max = {T_max:7.1f} K, T_frz = {T_freeze:7.1f} K, N_H < 1e23
        """.format(T_min=T_min, T_max=T_max, T_freeze=T_freeze)

        warm_cols = {}
        for species in selected_species:
            warm_cols.update({species:sum(dz[warm]*dataset['n'][species][warm,-1])})
            ostr += "\n \t{species:10s}: {wc:12.2e} [cm2]".format(species=species, 
                                                                wc=warm_cols[species])
        self.warm_column_data = ostr

    def _load_button_fired(self):
        wildcard = 'DiSKAPP zip archive (*.zip) |*.zip|' + \
                   'DiSKAPP numpy array (*.npy) |*.npy|' 

        dialog = FileDialog(action="open", wildcard=wildcard)
        dialog.open()
        if dialog.return_code == OK:
            
            if self.dataloaded :
                for ax in self.figure.axes :
                    for i in xrange(len(ax.lines)) : ax.lines.pop(0)
                self.suptitle._visible = False

            self.dataloaded = True
            self.filename = dialog.filename
            
            zf = zipfile.ZipFile(dialog.path)            

            # if 'reactions.py' in zf.namelist() :
            #     global reactions
            #     importer = zipimport.zipimporter(dialog.path)
            #     reactions = importer.load_module('reactions')

            numpyfile = [f for f in zf.namelist() if f.lower().endswith('.npy')]
            if numpyfile :
                zfdata = zf.read(numpyfile[0])
                d = np.load(BytesIO(zfdata),  encoding="latin1", allow_pickle=True).item()
                #d = np.load(numpyfile[0], allow_pickle=True).item()
                self.data = d

                for x in d['therm'].dtype.names:
                    d['therm'][x][np.where(d['therm'][x] < eps)] = np.nan

                self.species  = list(d['n'].dtype.names)
                self.therm_proc = list(d['therm'].dtype.names)
                self.nHlow  = np.log10(min(d['N_H']))
                self.nHhigh = np.log10(max(d['N_H']))  
                self.tclow  = np.log10(min(d['t'])/yr)
                self.tchigh = np.log10(max(d['t'])/yr)

                self.chem_time = self.tchigh
            else :
                print("Warning: numpy file not found in zip archive.")

            logfile = [f for f in zf.namelist() if f.lower().endswith('.log')]
            if logfile :
                self.logfile = logfile[0]
                tmp_log = zf.read(logfile[0]).decode('UTF-8')
                self.log = tmp_log
                self.radius = self.data['radius']
            else :
                print("Warning: dataset log file not found in zip archive.")
                self.radius = 0

            self.dataset_show()
            rxnfile = [f for f in zf.namelist() if f.lower().endswith('.rxn')]
            zf.close()
             
class MainWindow(HasTraits):
    """ The main window, here go the instructions to create and destroy
        the application.
    """
    figure = Instance(Figure)
    panel = Instance(ControlPanel)

    def _figure_default(self):

        fs = 14
        xlim = (19,23)
        
        figure = Figure()
        figure.subplots_adjust(left=0.10, bottom=0.07,right=0.95,
                               top=0.95, wspace=0.24, hspace=0)

        ax   = figure.add_subplot(121, position=[0.15,0.36,0.80,0.60])
        ax1  = figure.add_subplot(122, position=[0.15,0.05,0.80,0.29], sharex=ax)

        ax.set_ylabel('abundance, log($x$)', fontsize=fs)
        ax.set_xlim(xlim)
        ax.grid(which='both')
        setp(ax.get_xticklabels(), visible=False)
        
        ax1.set_ylabel('temperature [K]', fontsize=fs)
        ax1.set_xlim(xlim); ax1.set_ylim((0,7000))
        ax1.set_xlabel('vertical column density, log($N_H$)', fontsize=fs)
        ax1.grid(which='both')

        return figure

    def _panel_default(self):
        return ControlPanel(figure=self.figure)

    view = View(HSplit(Item('figure', 
                            editor=MPLFigureEditor(), 
                            dock='vertical'),
                       Item('panel',
                            style="custom",
                            resizable=True,
                            ),
                       show_labels=False, 
                       ),
                title="DiSKAPP v0.9.4",
                resizable=True, 
                height=0.8, width=0.8,
                )       

