import click

from .diskapp import MainWindow

@click.command(context_settings={'show_default': True})
def cli() -> None:
    """
    PLACEHOLDER FOR DISKAPP GUI
    """
    MainWindow().configure_traits()


if __name__ == "__main__":
    cli()
