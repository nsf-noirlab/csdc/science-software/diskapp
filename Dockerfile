FROM python:3.12-slim as base

# python
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    POETRY_NO_INTERACTION=1


WORKDIR /app
COPY ./ ./

RUN pip install ./

ENTRYPOINT ["vstruct"]
