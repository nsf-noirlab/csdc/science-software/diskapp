import logging
import os
import re
import sys
import time
import tomllib
import zipfile
from importlib.metadata import version as get_version
from shutil import copy2

from . import cooling
from . import heating
from .dust import set_dust_properties
from .log_utils import LOG_HEADER

import click
import numpy as np


VALID_RADII = (0.1, 0.24, 0.48, 0.95, 2.11, 5.25, 10.39, 10.4)

def vstruct(config: dict):

    # TODO: function to validate config dict
    run_id = config["run_id"]
    input_radius = config["radius"]
    zmin = config["zmin"]
    zmax = config["zmax"]
    t_0 = config["t_0"]
    t_max = config["t_max"]
    n_t = config["n_t"]
    imax = config["imax"]
    n_thresh = config["n_thresh"]
    oxygen_depletion = config["oxygen_depletion"]
    carbon_depletion = config["carbon_depletion"]
    FUV_luminosity = config["FUV_luminosity"]
    LyaPerFUVContinuum = config['LyaPerFUVContinuum']

    if input_radius not in VALID_RADII:
        raise ValueError(f"Invalid Radius, valid values are {VALID_RADII}")

    version = get_version("vstruct-diskapp")

    fi = lambda array, value : (np.abs(array-value)).argmin()

    # Physical constants
    R_Sun  = 6.96e10          # Solar radius, [cm]
    L_Sun  = 3.845e33         # Solar luminosity, [erg/cm/s] 
    c      = 2.9979e10        # speed if light, [cm s-1]
    h      = 6.6261e-27       # Planck constant, [erg s]
    mH     = 1.6733e-24       # mass of H atom [g]
    AUcm   = 1.496e13         # unit conversion from [AU] to [cm]
    ergpeV = 1.602e-12        # unit conversion [ergs per eV]
    day    = 60*60*24.0       # seconds per day
    yr     = 60*60*24*365.25  # seconds per year
    pi     = 3.141592653589793

    structure_file = 'disk_structure.r79_z480.npy'
    xray_rate_file = 'ionization_rates.EG13.COUP_depleted_R{:4.2f}AU.npy'.format(input_radius)
    visser_file    = 'Visser2009_shield.03.100.69-557-36.npy'
    li_file        = 'Li2013.npy'

    dirname = os.path.dirname(__file__)
    path = os.path.join(dirname, 'disk')
    path_out = os.getcwd()


    if os.path.isdir(path):
        disk_structure = np.load(os.path.join(path, 'data', 'structure', structure_file), encoding="latin1", allow_pickle=True).view(np.recarray)
        xray_rates     = np.load(os.path.join(path, 'data', 'xray', xray_rate_file), encoding="latin1", allow_pickle=True).item()
        visser_data    = np.load(os.path.join(path, 'data', 'FUV' , visser_file), encoding="latin1", allow_pickle=True).item()
        li_data        = np.load(os.path.join(path, 'data', 'FUV', li_file), encoding="latin1", allow_pickle=True).item()
    else:
        sys.exit('ERROR: data path not found: '+path)

    # Setup output naming and logging 
    r = ir  = fi(disk_structure.r[:,-1], input_radius)
    radius  = disk_structure.r[ir,-1]

    file_id     = f"{run_id}_v{version}_r{input_radius}AU"
    output_file = f"vstruct_{file_id}"
    log_file1   = f"{output_file}.log"
    log_file2   = f"{output_file}.debug.log"
    zip_file    = f"{output_file}.zip"

    logger = logging.getLogger()
    logger.setLevel('DEBUG')

    logformat   = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    filehandler1 = logging.FileHandler(log_file1)
    filehandler1.setFormatter(logformat)
    filehandler1.setLevel('INFO')
    logger.addHandler(filehandler1)    

    filehandler2 = logging.FileHandler(log_file2)
    filehandler2.setFormatter(logformat)
    filehandler2.setLevel('DEBUG')
    logger.addHandler(filehandler2)    

    # Define FUV bands, molecular cross-sections & ISM rates for scaling
    band        = np.linspace(950,2250,14)
    L_FUV       = FUV_luminosity #0.0025
    L_FUV_band  = (L_FUV*L_Sun/9.)*np.ones(len(band))
    U_FUV_ISM   = 4.6e-17
    F_band_ISM  = c*U_FUV_ISM*200
    n_band_ISM  = F_band_ISM/(h*c/(1000./1e8))
    G_H2_ISM    = 4.3e-11 
    G_CO_ISM    = 2.0e-10
    G_N2_ISM    = 1.0e-10
    G_C_ISM     = 2.0e-10

    sigma_species = ['H2O','OH','O2',
                     'NH3','HCN',
                     'C','CH4','C2H2',
                     'S','SO','SO2','OCS','H2S',
                     ]
    dtype_sigma = dict(names=sigma_species, formats=[float]*len(sigma_species))

    N_species_list = ['H','H2','O','CO','N2','Si'] + sigma_species
    dtype_N = dict(names=N_species_list, formats=[float]*len(N_species_list))

    sigma = np.ndarray( len(band), dtype=dtype_sigma)
    sigma_Lya = np.ndarray(1, dtype=dtype_sigma)

    #                         950  1050 1150 1250 1350 1450 1550 1650 1750 1850 1950 2050 2150 2250 
    sigma['H2O']  = np.array([22.0, 8.0, 6.0, 8.0, 3.5, 0.8, 2.4,4.9, 3.0,0.07,.002,  0,   0,  0])*1e-18
    sigma['OH']   = np.array([0.42, 8.4, 2.8, 1.8, 0.7, 1.6, 3.0,2.0, 0.5,0.05,.002,  0,   0,  0])*1e-18
    sigma['O2']   = np.array([ 7.0, 1.8, 0.5, 0.5, 5.0, 15., 9.0,2.0, .25,.006, 0.0,  0,   0,  0])*1e-18
    sigma['NH3']  = np.array([ 0.0, 7.1,14.4, 8.5, 8.8, 2.9, 1.4,0.8, 2.7, 6.4, 8.0,  0,   0,  0])*1e-18
    sigma['HCN']  = np.array([ 0.0,15.7,40.0,10.0, 4.0, 1.8, 0.3,0.1,0.04,   0,   0,  0,   0,  0])*1e-18
    sigma['C']    = np.array([16.0,16.0,   0,   0,   0,   0,   0,  0,   0,   0,   0,  0,   0,  0])*1e-18
    sigma['CH4']  = np.array([ 0.0, 9.4,18.1,18.6, 7.3,0.11,   0,  0,   0,   0,   0,  0,   0,  0])*1e-18
    sigma['C2H2'] = np.array([ 0.0,14.8,29.8,35.1,44.8,15.0,21.1,1.1, 1.2, 0.5, 0.2,  0,   0,  0])*1e-18
    sigma['S']    = np.array([  75,  55,  50,   0,   0,   0,   0,  0,   0,   0,   0,  0,   0,  0])*1e-18
    sigma['SO']   = np.array([   0,  80, 100, 125,  30,   3,   3,  3,   3,   5,  13,  10,  5,  1])*1e-18
    sigma['SO2']  = np.array([   0, 5.9,15.2,51.3, 6.8, 4.3, 3.5,0.6, 1.2, 4.0, 6.7, 6.0,3.0,0.8])*1e-18
    sigma['OCS']  = np.array([  75,  50,  30,  29,  28,  40,  55, 35,  15,   5,   2,0.07,.24,.31])*1e-18
    sigma['H2S']  = np.array([  75,  50,  30,  29,  28,  40,  55, 35,  15,   5,   2,0.07,.24,.31])*1e-18

    # From van Dishoeck et al., 2006 - Table 1
    sigma_Lya = {'H2O': 1.2e-17,
                 'OH':  1.8e-18, 
                 'O2':  1.0e-20,
                 'NH3': 1.0e-17,
                 'HCN': 3.0e-17,
                 'CH4': 1.8e-17,
                 'C2H2':4.0e-17,
                 'SO':  1.0e-16,
                 'SO2': 4.0e-17,
                 'OCS': 3.0e-17,
                 'H2S': 3.0e-17,
                 'Si':  3.0e-17,
                 }

    sigma_table = "Band (A)"
    for x in sigma_species : sigma_table += "{:^8s}".format(x)
    sigma_table += "\n"

    for i,w in enumerate(band):
        sigma_table += "{:^8.0f}".format(w)
        for x in sigma_species : 
            sig = sigma[x][i]*1e18
            if sig < 0.1 and sig > 0:
                sigma_table += "{:^8.3f}".format(sig)
            else :
                sigma_table += "{:^8.1f}".format(sig)
        sigma_table += "\n"

    def abundances():
        """Return elemental abundances for solar composition and depleted
           abundances for disk."""

        x = ['H','He','C','N','O','Ne','Na','Mg','Al','Si','P',
             'S','Cl','Ar','K','Ca', 'Ti','Cr','Mn','Fe','Co','Ni']
        dtype = {'names':x,'formats':['<f8']*len(x)}
        x_solar = np.rec.array([1.0, 0.1, 2.5e-4, 6.0e-5, 4.5e-4, 6.9e-5, 1.5e-6, 
            3.4e-5, 2.3e-6, 3.2e-5, 2.3e-7, 1.4e-5, 3.2e-7, 1.5e-6, 1.2e-7, 2.0e-6,
            6.5e-8, 4.4e-7, 2.5e-7, 2.8e-5, 8.3e-8, 1.7e-6], 
            dtype=dtype )
        x_disk = np.rec.array([  
            1.0,    0.1, 2.5e-4/carbon_depletion, 6.0e-5, 4.5e-4/oxygen_depletion, 6.9e-5, 1.5e-6/6.5, 3.4e-5/32, 
            2.3e-6, 3.2e-5/190, 2.3e-7, 1.4e-5/50, 3.2e-7, 1.5e-6/1.3, 1.2e-7/14, 2.0e-6,
            6.5e-8, 4.4e-7, 2.5e-7, 2.8e-5/160, 8.3e-8, 1.7e-6], 
            dtype=dtype )    

        return x_solar, x_disk


    def FUV_number_flux(L_FUV, N, radius, tau_dust, sigma, band, column_ratio, verbose=False):
        """
        FUV photon number flux calculation. Assumes the overlying 
        vertcal columns of species are proportional to the line 
        of sight (los) columns. 

        Inputs:

        L_FUV    - FUV luminosity from 1200-2000A, [L_FUV/L_Sun] 
        N        - vertical column to middle of grid point [cm-2]
        radius   - radial distance to grid point [AU]
        tau_dust - dust optical depth
        sigma    - molecular opacities, [cm2]
        band     - FUV band centers, [A]
        column_ratio - N_los(x)/N(x)

        Outputs:

        n_FUV    - FUV number flux per 100A band
        F_FUV    - number flux from 1100-1550A, for photoelectric heating.

        """
        flux  = lambda r, L:  L/(4*pi*(r*AUcm)**2)           # FUV flux [erg cm-2 s-1]
        hnu   = lambda angstrom=1000.: (h*c)/(angstrom*1e-8) # photon energy [erg]
        n_FUV = np.zeros(len(band))

        for i,wavelength in enumerate(band):
            f_0 = flux(radius, L_FUV_band[i])/hnu(angstrom=wavelength)
            tau_mol = 0.0
            for x in sigma_species: 
                tau_mol += sigma[x][i]*N[x]*column_ratio
            n_FUV[i] = f_0*np.exp(-1*(tau_mol+tau_dust))

        F_FUV = float(np.sum(n_FUV[np.where(band<1500)]) 
                          + 0.5*n_FUV[np.where(band==1550)])

        if verbose:
            logstr =  "\n    continuum opacity:\n"
            logstr += "      line of sight dust opacity ....... tau_dust = {:8.2e}\n".format(tau_dust)
            for x in sigma_species: 
                logstr += "      l.o.s. column of {:s} ".format(x) + '.'*(17-2*len(x)) + \
                          " N_los[{:s}] = {:8.2e} [cm-2]\n".format(x, float(N[x]*column_ratio))
            logger.info(logstr)

        return n_FUV, F_FUV

    def local_G_FUV(sigma, n_FUV, n_Lya, f_sc, N, column_ratio, verbose=False):
        """Calculate FUV photorates, G_FUV, for each species. Special cases 
        added for H2, CO, N2, and C.
        Assumes that first two elements of number flux array, n_FUV[0:2], 
        correspond to the 900-1000A and 1000-1100A bands.
        """

        f_ISM = np.sum(n_FUV[0:2])/n_band_ISM 

        G_FUV_species = sigma_species + ['H2','CO','N2',
                                         'H2O_Lya','OH_Lya',
                                         'NH3_Lya','HCN_Lya',
                                         'CH4_Lya','C2H2_Lya',
                                         'SO_Lya','SO2_Lya','OCS_Lya','H2S_Lya',
                                         'Si_Lya',
                                         ]
        dtype_G_FUV = dict(names=G_FUV_species, 
                           formats=[float]*len(G_FUV_species))
        G_FUV = np.zeros(1, dtype=dtype_G_FUV)
        N_los = np.zeros(1, dtype=N.dtype)

        for x in N_los.dtype.names : N_los[x] = N[x]*column_ratio

        J = lambda x,alpha,b5: 0.965/((1+(x/b5))**alpha) + (0.035/(1+x)**0.5)*np.exp(-8.5e-4*(1+x)**0.5)
        J_H2 = J(x=(N_los['H2'])/5e14, alpha=1.1, b5=3.0)

        i,j = fi(visser_data['N_CO'], N_los['CO']), fi(visser_data['N_H2'], N_los['H2']) 
        J_CO = visser_data['J_CO'][i,j]

        n,m = fi(li_data['N_H2'], N_los['H2']), fi(li_data['N_N2'], N_los['N2'])
        J_N2 = li_data['3km/s']['1000K'][1e22][n,m]

        G_FUV['H2'] = G_H2_ISM * f_ISM * J_H2
        G_FUV['CO'] = G_CO_ISM * f_ISM * J_CO
        G_FUV['N2'] = G_N2_ISM * f_ISM * J_N2
        G_FUV['C']  = G_C_ISM  * f_ISM * J_H2

        G_FUV['H2O_Lya']  = sigma_Lya['H2O'] *n_Lya*f_sc
        G_FUV['OH_Lya']   = sigma_Lya['OH']  *n_Lya*f_sc
        G_FUV['NH3_Lya']  = sigma_Lya['NH3'] *n_Lya*f_sc
        G_FUV['HCN_Lya']  = sigma_Lya['HCN'] *n_Lya*f_sc
        G_FUV['CH4_Lya']  = sigma_Lya['CH4'] *n_Lya*f_sc
        G_FUV['C2H2_Lya'] = sigma_Lya['C2H2']*n_Lya*f_sc
        G_FUV['SO_Lya']   = sigma_Lya['SO']  *n_Lya*f_sc
        G_FUV['SO2_Lya']  = sigma_Lya['SO2'] *n_Lya*f_sc
        G_FUV['OCS_Lya']  = sigma_Lya['OCS'] *n_Lya*f_sc
        G_FUV['H2S_Lya']  = sigma_Lya['H2S'] *n_Lya*f_sc
        G_FUV['Si_Lya']   = sigma_Lya['Si']  *n_Lya*f_sc

        n_shielded = np.copy(n_FUV)
        n_shielded[0:2] *= J_H2

        if verbose:
            logger.info(log_shielding.format(
                f_ISM=float(f_ISM), 
                N_H =float(N_los['H']), 
                N_H2=float(N_los['H2']), 
                N_CO=float(N_los['CO']), 
                N_N2=float(N_los['N2']), 
                J_H2=float(J_H2), 
                J_CO=float(J_CO), 
                J_N2=float(J_N2), 
                J_C =float(J_H2), 
                ))

        for x in sigma_species : 
            G_FUV[x] = np.sum(sigma[x]*n_shielded)

        return G_FUV

    log_init = """
    {pybin}
    {pyversion}

    Files used:
        Current directory... {pwd} 
        Reaction network.... {reactionfile}
        Kinetics module..... {kineticsmodule}
            version {kinetics_version}
        Reaction module..... {reactionmodule}
        Disk structure...... {structure_file}
        X-ray rates......... {xray_rate_file}
        Output npy data..... {npyfile}

    Radial distance: {radius:5.2f}[AU]

    Vertical column range:
        user limits: {zmin:7.1e} -- {zmax:7.1e} [cm-2]
        calc limits: {zminc:7.1e} -- {zmaxc:7.1e} [cm-2]
        column indices: {iz_min:d} - {iz_max:d} (from disk_structure)
        total vertical steps: {step_total}

    Kinetics integration timescales:
        t_0:  {t_0s:8.2e} [s] {t_0y:8.2e} [yr]
        t_max:{t_mxs:8.2e} [s] {t_mxy:8.2e} [yr]

    Elemental abundances:
    {abundance_table}

    FUV fluxes and rates:
        ISM FUV energy density ............ U_FUV_ISM = {U_FUV_ISM:8.1e} [erg cm-3 A-1]
        ISM flux in 100A band ............ F_band_ISM = {F_band_ISM:8.1e} [erg cm-2 s-1] 
        ISM number flux (900-1100A) ...... n_band_ISM = {n_band_ISM:8.2e} [cm-3 s-1]
        ISM H2 photo-rate .................. G_H2_ISM = {G_H2_ISM:8.2e} [s-1] 
        ISM CO photo-rate .................. G_CO_ISM = {G_CO_ISM:8.2e} [s-1] 
        ISM N2 photo-rate .................. G_N2_ISM = {G_N2_ISM:8.2e} [s-1] 
        ISM C photo-rate .................... G_C_ISM = {G_C_ISM:8.2e} [s-1]

    FUV molecular cross sections [1e-18 cm2]:
    {sigma_table}
    """

    log_gridcell ="""
    Grid cell properties:
        grid indices .......................... (r,z) = ({r:d},{z:d})
        radial distance ........................... R = {radius:8.3f} [AU]
        altitude .............................. z_alt = {alt: 8.3f} [AU]
                  ........................... z_alt/R = {rat:8.3f}
        local density ............................ nH = {nH:8.2e} [cm-3]
        initial temperatures .................. T_gas = {T_gas:8.1f} [K]
                             ................. T_dust = {T_dust:8.1f} [K]
        vertical column ......................... N_H = {N_H:8.2e} [cm-2]
        line of sight hydrogen column ....... N_H_los = {N_H_los:8.2e} [cm-2]
        column density ratio .............. N_los/N_H = {column_ratio:8.2e}

        X-ray ionization rate ........... zeta(total) = {zeta_total:8.2e} [s-1]

    """

    log_shielding ="""
        Shielding functions (900-1100A):
          FUV flux relative to ISM ............ f_ISM = {f_ISM:8.2e}
             (accounting for all continuum attenuation)
          line of sight column of H  ...... N_los[H]  = {N_H:8.2e} [cm-2]
          line of sight column of H2 ...... N_los[H2] = {N_H2:8.2e} [cm-2]
          line of sight column of CO ...... N_los[CO] = {N_CO:8.2e} [cm-2]
          line of sight column of N2 ...... N_los[N2] = {N_N2:8.2e} [cm-2]
          H2 shielding function (DB96) ........ J(H2) = {J_H2:8.2e}
          CO shielding function (VvDB09) ...... J(CO) = {J_CO:8.2e}
          N2 shielding function (Li2013) ...... J(N2) = {J_N2:8.2e}
          C  shielding function (J(C)=J(H2)) .. J(C)  = {J_C:8.2e}

    """

    log_photorates ="""
        Stellar FUV luminosity per band .. L_FUV_band = {L_FUV_band:8.2e} [erg/s/100A]
        FUV number flux, (900-2300A) .......... n_FUV = {FUV1:8.2e} [cm-2/s]
                         (900-1100A) .......... n_FUV = {FUV2:8.2e} [cm-2/s]
          for dust phel (1100-1550A) .......... F_FUV = {F_FUV:8.2e} [cm-2/s]

        photorates:
          H2 .............................. G_FUV(H2) = {G_FUV_H2:8.2e} [cm-3/s]
          CO .............................. G_FUV(CO) = {G_FUV_CO:8.2e} [cm-3/s]
          N2 .............................. G_FUV(N2) = {G_FUV_N2:8.2e} [cm-3/s]
          C ................................ G_FUV(C) = {G_FUV_C:8.2e} [cm-3/s]
          H2O ............................ G_FUV(H2O) = {G_FUV_H2O:8.2e} [cm-3/s]
          OH .............................. G_FUV(OH) = {G_FUV_OH:8.2e} [cm-3/s]
          CH4  ........................... G_FUV(CH4) = {G_FUV_CH4:8.2e} [cm-3/s]
          C2H2 .......................... G_FUV(C2H2) = {G_FUV_C2H2:8.2e} [cm-3/s]
          HCN ............................ G_FUV(HCN) = {G_FUV_HCN:8.2e} [cm-3/s]
          NH3 ............................ G_FUV(NH3) = {G_FUV_NH3:8.2e} [cm-3/s]

        Ly-a photorates:
          H2O ............................ G_Lya(H2O) = {G_LYA_H2O:8.2e} [cm-3/s]
          OH .............................. G_Lya(OH) = {G_LYA_OH:8.2e} [cm-3/s]
          scattering scale factor .............. f_sc = {f_sc:8.2f}

    """


    def vertical_structure(r, disk_structure, xray_rates, zlimits=(1e19, 1e26),
                           t_0=4, t_max=11, n_t=80, imax=20, n_thresh=0.001) :    
        """
        Use the thermal-chemical kinetics integration to calculate the
        vertical structure at a particular radius, r.
        """
        t_start = time.time()
        logger.info(LOG_HEADER.format(L_FUV=L_FUV, L_FUV_band=L_FUV_band[-1]))

        iz_min = max(0, fi(disk_structure.N_H[r,:],zlimits[0])-1)
        iz_max = min(len(disk_structure.N_H[r,:])-2,
                     fi(disk_structure.N_H[r,:],zlimits[1])+1)

        step_total = iz_max - iz_min + 1
        z  = iz_min
        dz = (disk_structure.z[r,z-1]-disk_structure.z[r,z])*1.5e13
        nH = disk_structure.n[r,z]

        i_zeta = fi(xray_rates['N_H'], disk_structure.N_H[r,z])  

        t_chem = np.logspace(t_0,t_max,n_t)
        x_solar, x_disk = abundances()

        n0_dict = {}
        for x in x_disk.dtype.names :
            n0_dict[x] = float(x_disk[x])*disk_structure.n[r,z]
        for species in ['T','T_d'] : 
            n0_dict.update({species: disk_structure.T_dust[r,z]})
        n0_dict['H2'] = 0.0

        abundance_table = "   Species | x_solar  |  x_disk  | depletion\n"
        for A in x_solar.dtype.names :
            abundance_table += ("{:^11s}|{:9.2e} |{:9.2e} |{:8.1f}\n".format(A,  
               float(x_solar[A]), float(x_disk[A]),float(x_solar[A]/x_disk[A])))

        logger.info(log_init.format(pyversion=sys.version, pybin=sys.executable, pwd=os.getcwd(),
             reactionfile=reactionfile, reactionmodule=reactionmodule,npyfile=output_file+'.npy',
             kineticsmodule=kineticsmodule, kinetics_version=kinetics.__version__, z=z,
             structure_file=structure_file, xray_rate_file=xray_rate_file, 
             radius=disk_structure.r[r,z], step_total=step_total,zmin=zmin,zmax=zmax,
             zminc=disk_structure.N_H[r,iz_min], zmaxc=disk_structure.N_H[r,iz_max],iz_min=iz_min,
             iz_max=iz_max, t_0s=10**t_0,t_0y=(10**t_0)/yr,t_mxs=10**t_max,t_mxy=(10**t_max)/yr,
             abundance_table=abundance_table, sigma_table=sigma_table, U_FUV_ISM =U_FUV_ISM, 
             F_band_ISM=F_band_ISM, n_band_ISM=n_band_ISM, G_H2_ISM=G_H2_ISM, G_C_ISM=G_C_ISM,
             G_N2_ISM=G_N2_ISM, G_CO_ISM=G_CO_ISM,
             ))

        dust = set_dust_properties(a_g=0.7070, Q_abs=1.0, Q_ext=1.0, rho_bar=3,
                                   dust_gas_ratio=0.01, Y=0.05, E_phel=5.3)    
        logger.info(dust['properties'].format(**dust))

        tau_dust = dust['sigma']*disk_structure.N_H_los[r,z]
        column_ratio = disk_structure.N_H_los[r,z]/disk_structure.N_H[r,z]

        logger.info(log_gridcell.format(radius=radius, r=r, z=z, alt=disk_structure.z[r,z],
            rat=float(disk_structure.z[r,z]/radius), T_gas=n0_dict['T'], T_dust=n0_dict['T_d'], 
            nH=nH, N_H=disk_structure.N_H[r,z], N_H_los=disk_structure.N_H_los[r,z],
            zeta_total=xray_rates['zeta']['total'][i_zeta],
            tau_dust=tau_dust, column_ratio=float(column_ratio),
            ))

        N_over = np.zeros(1, dtype=dtype_N).view(np.recarray)
        dN     = np.zeros(1, dtype=dtype_N).view(np.recarray)
        N      = np.ones(1,  dtype=dtype_N).view(np.recarray)

        n_FUV, F_FUV = FUV_number_flux(L_FUV, N, radius, tau_dust, sigma, band, 
                                       column_ratio, verbose=True)

        ### Set Downward Lyman-alpha from FUV continuum ###
        sigma_HI_sc = 1e-20  # Ly-a scattering cross section.
        tau_Lya = 0. # effective optical depth of Lya including scattering (cumulative)
        f_sc = 1.0

        n_Lya_per_FUV = LyaPerFUVContinuum #3
        n_Lya_0 = np.sum(n_FUV[2:7]) * n_Lya_per_FUV
        n_Lya = np.sum(n_FUV[2:7]) * n_Lya_per_FUV

        F_FUV = float(np.sum(n_FUV[np.where(band<1500)])+0.5*n_FUV[np.where(band==1550)]+n_Lya*f_sc) 
        G_FUV = local_G_FUV(sigma, n_FUV, n_Lya, f_sc, N, column_ratio, verbose=True)

        logger.info(log_photorates.format(
            L_FUV_band=L_FUV_band[-1], F_FUV=F_FUV, FUV1=float(np.sum(n_FUV)), 
            FUV2=float(np.sum(n_FUV[0:2])), 
            G_FUV_H2=float(G_FUV['H2']), G_FUV_CO=float(G_FUV['CO']), G_FUV_C=float(G_FUV['C']),
            G_FUV_H2O=float(G_FUV['H2O']), G_FUV_OH=float(G_FUV['OH']), G_FUV_CH4=float(G_FUV['CH4']),
            G_FUV_C2H2=float(G_FUV['C2H2']), G_FUV_HCN=float(G_FUV['HCN']), G_FUV_NH3=float(G_FUV['NH3']),
            G_FUV_N2=float(G_FUV['N2']), G_LYA_H2O=float(G_FUV['H2O_Lya']), G_LYA_OH=float(G_FUV['OH_Lya']),
            f_sc=f_sc,
            ))

        logger.info("    Downward Ly-a flux at top: {:8.2e} [cm-2/s]".format(n_Lya_0))

        try :
            initial_conditions_path = os.path.join(path_out, f'tmp_{output_file}.init.npy')
            data = np.load(initial_conditions_path).item()
            logger.info("\nRestored initial conditions: \n"+35*" "+initial_conditions_path)
        except :
            logger.info("Running initial cell calculation...")   

            tol  = 1e-10 ; mxstep = 1600
            data = kinetics.integrate_chem(n0_dict, t_chem, xray_rates['zeta'][i_zeta],
                               disk_structure.r[r,z], N[0], G_FUV, F_FUV, 
                               dust, dz=dz, output_time_evolution=True, 
                               rtol=tol, atol=tol, mxstep=mxstep, full_output=True)

            t_calc = time.time() - t_start
            logger.info("Initial (atomic) cell calculation time:{:6.1f} [min]".format(t_calc/60.))
            save_conditions_path = os.path.join(path_out, f'tmp_{output_file}.init')
            np.save(save_conditions_path, data)

            logger.debug("H balance: n_H={:9.3E}, n(H)={:9.3E}, n(H2)={:9.3E}, n(tot)={:9.3E}".format(
               nH, data['n']['H'][-1], data['n']['H2'][-1], data['n']['H'][-1]+2*data['n']['H2'][-1]))
            logger.info("\nSaved initial conditions: \n\t "+save_conditions_path)

        step  = 0
        log_vdata ="""\nCreating vertical data structure:\n"""
        vdata = {'version':version,
                 'radius':radius,
                 'z':np.ndarray(step_total),
                 'N_H':np.ndarray(step_total),
                 'N_H_los':np.ndarray(step_total),
                 'tau_dust':np.ndarray(step_total),
                 'n_FUV':np.ndarray((step_total,len(n_FUV))),
                 'n_Lya':np.ndarray(step_total),
                 'tau_Lya':np.ndarray(step_total),
                 'dtau_Lya':np.ndarray(step_total),
                 'tau_dust_vert':np.ndarray(step_total),
                 'tau_HI_sc':np.ndarray(step_total),
                 'f_sc':np.ndarray(step_total),
                 'dust':dust, 'L_FUV':L_FUV, 'band':band, 't':t_chem, 'itg_info':[]}

        vdata['z'][step] = disk_structure.z[r,z]
        vdata['N_H'][step] = disk_structure.N_H[r,z]
        vdata['N_H_los'][step] = disk_structure.N_H_los[r,z]
        vdata['tau_dust'][step] = tau_dust
        vdata['n_FUV'][step,:] = n_FUV
        vdata['n_Lya'][step] = n_Lya
        vdata['tau_Lya'][step] = tau_Lya

        vdata.update({'n':np.ndarray((step_total,len(data['n'])),dtype=data['n'].dtype)})
        vdata['n'][step,:] = data['n']
        log_vdata += "Re-dimensioned: {key} to ({steps},{len}) ndarray\n".format(
                    key='n', steps=step_total, len=len(data['n']))

        for key in ['dz','nH','zeta','N','F_FUV','G_FUV',]:
            try:
                vdata.update({key:np.ndarray(step_total,dtype=data[key].dtype)})
            except:
                vdata.update({key:np.ndarray(step_total)})
            vdata[key][step] = data[key]
            log_vdata += "Re-dimensioned: {key} to ({steps}) ndarray\n".format(
                          key=key, steps=step_total)
        vdata['itg_info'].append(data['info'])

        logger.info(log_vdata)

        i = -1    
        logger.debug(("{:>3s}"+8*"{:^10s}").format(
            'i', 'N(H2O)', 'N^(H2O)', 'dN(H2O)', 'G_FUV(H2O)',
            'N(OH)', 'N^(OH)', 'dN(OH)', 'G_FUV(OH)' ))
        logger.debug(("{:3d}"+8*"{:10.3e}").format(
                i,  N['H2O'][0], N_over['H2O'][0], dN['H2O'][0], G_FUV['H2O'][0],
                N['OH'][0], N_over['OH'][0], dN['OH'][0], G_FUV['OH'][0] ))

        for z in range(iz_min+1,iz_max+1):

            t_gridcell = time.time() ; i = 0 ; step = z - iz_min
            nH = disk_structure.n[r,z]
            nH_scale = disk_structure.n[r,z] / disk_structure.n[r,z-1]

            n0_dict = {} 
            data_in = data
            for species in data['n'].dtype.names : 
                n0_dict.update({species: data['n'][species][-1]*nH_scale})
                if (species != 'T') and (species != 'T_d') :
                    data_in['n'][species] *= nH_scale

            n0_dict['T']   = data['n']['T'][-1]
            n0_dict['T_d'] = disk_structure.T_dust[r,z]
            dz = (disk_structure.z[r,z-1]-disk_structure.z[r,z])*1.5e13

            logger.debug("H balance: n_H={:9.3E}, n(H)={:9.3E}, n(H2)={:9.3E}, n(tot)={:9.3E}".format(
               nH, data['n']['H'][-1], data['n']['H2'][-1], data['n']['H'][-1]+2*data['n']['H2'][-1]))

            i_zeta = fi(xray_rates['N_H'], disk_structure.N_H[r,z])
            tau_dust = dust['sigma']*disk_structure.N_H_los[r,z]
            column_ratio = disk_structure.N_H_los[r,z]/disk_structure.N_H[r,z]

            logger.info(log_gridcell.format(radius=radius, r=r, z=z, alt=disk_structure.z[r,z],
                rat=float(disk_structure.z[r,z]/radius), T_gas=n0_dict['T'], T_dust=n0_dict['T_d'], 
                nH=nH, N_H=disk_structure.N_H[r,z], N_H_los=disk_structure.N_H_los[r,z],
                zeta_total=xray_rates['zeta']['total'][i_zeta],
                tau_dust=tau_dust, column_ratio=float(column_ratio),
                ))

            N_over = np.copy(N)                
            for species in N.dtype.names :
                dN[species] = n0_dict[species]*dz
                N[species]  = N_over[species] + dN[species]

            n_FUV, F_FUV = FUV_number_flux(L_FUV, N, radius, tau_dust, sigma, band, column_ratio, verbose=True)

            logger.info('Initial column estimates:')
            logger.info(' -- assume abundances from prevoius grid point for 1/2 cell column')

            f_sc = 1.0
            tau_HI_sc = data['n']['H'][-1]*dz*sigma_HI_sc
            if tau_HI_sc > 1.0: f_sc = tau_HI_sc

            tau_dust_vert = dust['sigma']*disk_structure.N_H[r,z]*f_sc

            dtau_Lya = 0.0
            for x in sigma_Lya: 
                dtau_Lya += data['n'][x][-1]*sigma_Lya[x]*dz*f_sc
                logger.info('{:6s} vert column: {:8.2e}, Lya-band dtau_Lya: {:8.2e}'.format(
                    x, float(N[x]), float(data['n'][x][-1]*sigma_Lya[x]*dz)))

            n_Lya = float(n_Lya_0*np.exp(-1*(dtau_Lya+tau_Lya+tau_dust_vert)))

            logger.info("Initial downward Ly-a flux: {:8.2e} [cm-2/s]".format(n_Lya))
            F_FUV = float(np.sum(n_FUV[np.where(band<1500)]) + 0.5*n_FUV[np.where(band==1550)]+n_Lya*f_sc) 
            G_FUV = local_G_FUV(sigma, n_FUV, n_Lya, f_sc, N, column_ratio, verbose=True)

            logger.info(log_photorates.format(
                L_FUV_band=L_FUV_band[-1], F_FUV=F_FUV, FUV1=float(np.sum(n_FUV)), 
                FUV2=float(np.sum(n_FUV[0:2])), 
                G_FUV_H2=float(G_FUV['H2']), G_FUV_CO=float(G_FUV['CO']), G_FUV_C=float(G_FUV['C']),
                G_FUV_H2O=float(G_FUV['H2O']), G_FUV_OH=float(G_FUV['OH']), G_FUV_CH4=float(G_FUV['CH4']),
                G_FUV_C2H2=float(G_FUV['C2H2']), G_FUV_HCN=float(G_FUV['HCN']), G_FUV_NH3=float(G_FUV['NH3']),
                G_FUV_N2=float(G_FUV['N2']),  G_LYA_H2O=float(G_FUV['H2O_Lya']), G_LYA_OH=float(G_FUV['OH_Lya']),
                f_sc=f_sc,
                ))

            logger.debug("Iteration on photorates:")
            logger.debug(("{:>5s}"+9*"{:^10s}").format(
                'i', 'N(H2O)', 'N^(H2O)', 'dN(H2O)', 'G_FUV(H2O)',
                'N(OH)', 'N^(OH)', 'dN(OH)', 'G_FUV(OH)','error' ))
            logger.debug(("{:3d}"+8*"{:10.2e}").format(
                i,  N['H2O'][0], N_over['H2O'][0], dN['H2O'][0], G_FUV['H2O'][0],
                N['OH'][0], N_over['OH'][0], dN['OH'][0], G_FUV['OH'][0] ))

            tol  = 1e-10 ; mxstep = 1000 ; bad_count = 0 ; G_success = False

            for i in range(1,imax):  ## Iteration on photorate convergence. 
                G_in = G_FUV ; bad_integration = False

                try:
                    data = kinetics.integrate_chem(n0_dict, t_chem, xray_rates['zeta'][i_zeta], 
                                   disk_structure.r[r,z], N[0], G_FUV, F_FUV, 
                                   dust, dz=dz, output_time_evolution=True,
                                   rtol=tol, atol=tol, mxstep=mxstep, full_output=True)

                    logger.debug('\tkinetics.inetgrate_chem() returned in photorate loop.')

                    if data['info']['message'] == 'Integration successful.' :
                        for x in data['n'].dtype.names:
                           if not np.isfinite(data['n'][x]).all():
                              logger.warning('Bad value(s) found in {:s}.'.format(x))
                              bad_integration = True ; bad_count += 1
                              break
                        if not bad_integration :
                            for species in N.dtype.names :
                                dN[species] = data['n'][species][-1]*dz
                                N[species] = N_over[species] + dN[species]

                            n_FUV,F_FUV=FUV_number_flux(L_FUV,N,radius,tau_dust,sigma,band,column_ratio)

                            f_sc = 1
                            tau_HI_sc = data['n']['H'][-1]*dz*sigma_HI_sc
                            if tau_HI_sc > 1.0: f_sc = tau_HI_sc

                            dtau_Lya = 0.0
                            for x in sigma_Lya: 
                                dtau_Lya += data['n'][x][-1]*sigma_Lya[x]*dz*f_sc

                            n_Lya = float(n_Lya_0*np.exp(-1*(dtau_Lya+tau_Lya+tau_dust_vert)))

                            logger.info("Downward Ly-a flux: {:8.2e} [cm-2/s]".format(n_Lya))
                            F_FUV = float(np.sum(n_FUV[np.where(band<1500)]) 
                                                  + 0.5*n_FUV[np.where(band==1550)]+n_Lya*f_sc) 
                            logger.info("Recalculated F_FUV with Lya: {:8.2e} [cm-2/s]".format(F_FUV))

                            G_FUV = local_G_FUV(sigma, n_FUV, n_Lya, f_sc, N, column_ratio)

                            n_rel_error =  abs((G_in['H2O']-G_FUV['H2O'])/G_FUV['H2O']) + \
                                           abs((G_in['OH']-G_FUV['OH'])/G_FUV['OH'])
                            logger.info(("{:3d}"+9*"{:10.2e}").format(
                                i,  N['H2O'][0], N_over['H2O'][0], dN['H2O'][0], G_FUV['H2O'][0],
                                N['OH'][0],N_over['OH'][0],dN['OH'][0],G_FUV['OH'][0],n_rel_error[0]))
                            if n_rel_error < n_thresh :
                                pstr = "Photorates converged: G(H2O)={:8.2e} G(OH)={:8.2e}, error={:8.2e}"
                                logger.info(pstr.format(
                                    G_FUV['H2O'][0],G_FUV['OH'][0], n_rel_error[0]))
                                G_success = True
                                break
                            G_sum = 0.0
                            for x in G_FUV.dtype.names:
                                G_sum += G_FUV[x]
                            if G_sum < 1e-50:
                                logger.info('Photorates VERY small. Skipping convergence. Moving on.')
                                G_success = True
                                break
                        else :
                            logger.warning('Invalid data returned from integrator.')
                            tmp_file = 'tmp_{:s}.failed.z{:03d}.i{:03d}'.format(file_id, z, i)
                            np.save(os.path.join(path_out, tmp_file), data)
                            logger.info('Saved temporary file for gridpoint: {:s}'.format(tmp_file))
                            np.save(os.path.join(path_out,'tmp_{:s}.failed.z{:03d}.i{:03d}'.format(file_id,z,i)),data)
                            if bad_count > 3 :
                                logger.error('Maximum bad count reached. Exiting.')
                                return None, 'Failed.'
                            else :
                                logger.info("Resetting atomic conditions.")
                                x_solar, x_disk = abundances()
                                n0_dict = {}
                                for x in x_disk.dtype.names :
                                    n0_dict[x] = float(x_disk[x])*disk_structure.n[r,z]
                                for species in ['T','T_d'] :
                                    n0_dict.update({species: disk_structure.T_dust[r,z]})
                                n0_dict['H2'] = 0.0
                    else :
                        logger.warning('Integration NOT successful.')
                        tmp_file = 'tmp_{:s}.failed.z{:03d}.i{:03d}'.format(file_id, z, i)
                        np.save(os.path.join(path_out, tmp_file), data)
                        logger.info('Saved temporary file for gridpoint: {:s}'.format(tmp_file))
                        err_message =  'Excess work done on this call (perhaps wrong Dfun type).'
                        if data['info']['message'] == err_message :
                           tol *= 10
                           logger.info('Setting tolerance(s) to: {:6.1e}'.format(tol))
                except:
                    logger.error('Problem photorate loop. Storing zeros and moving to next gridpoint.')
                    tmp_file = 'tmp_{:s}.except.z{:03d}.i{:03d}'.format(file_id, z, i)
                    logger.error('Storing data object as: {:s}'.format(path_out+tmp_file))
                    np.save(os.path.join(path_out, tmp_file), data)
                    for species in data['n'].dtype.names : 
                        data['n'][species][:] = 0

            t_calc = time.time() - t_gridcell
            t_calc_tot = time.time() - t_start
            t_calc_avg = t_calc_tot / step
            t_remaining = t_calc_avg * (step_total - step)

            logger.info("Estimated completion: {:>30s}".format(      
                time.strftime("%I:%M%p - %d %B %Y", time.localtime(time.time()+t_remaining))))

            vdata['z'][step] = disk_structure.z[r,z]
            vdata['N_H'][step] = disk_structure.N_H[r,z]
            vdata['N_H_los'][step] = disk_structure.N_H_los[r,z]
            vdata['tau_dust'][step] = tau_dust
            vdata['n_FUV'][step,:] = n_FUV
            vdata['n_Lya'][step] = n_Lya
            vdata['tau_Lya'][step] = tau_Lya
            vdata['dtau_Lya'][step] = dtau_Lya
            vdata['tau_HI_sc'][step] = tau_HI_sc
            vdata['f_sc'][step] = f_sc
            vdata['tau_dust_vert'][step] = tau_dust_vert
            tau_Lya += dtau_Lya
            vdata['F_FUV'][step] = F_FUV
            vdata['G_FUV'][step] = G_FUV
            vdata['n'][step,:] = data['n']
            vdata['itg_info'].append(data['info'])        

            for key in ['dz','nH','N','zeta']:
                vdata[key][step] = data[key]
            np.save(os.path.join(path_out, f'tmp_{output_file}'), vdata)

            if G_success :            
                logger.debug("Photorates loop returned.")
            else :
                logger.info(
                    "Maximum iterations ({:d}) reached without converging or failure.".format(imax))

        return vdata, 'Vertical structure calculation complete.'

    def append_therm(data, verbose=False) :
        """Calculate heating and cooling rates for calculations performed with
        version 0.9.4 or later of the model.
        """

        version = get_version("vstruct-diskapp")
        radius  = data['radius']
        base_str = 'vert_struct_v0.9.5.{:02d}_r{:05.2f}AU'

        data.update({'alpha_h':heating.ALPHA_H})

        if verbose:
            pstr = " Model: radius={:5.2f}, alpha_h={:5.2f}\n Calculating thermal rates" 
            print(pstr.format(data['radius'], data['alpha_h']))

        n, N, G_FUV, radius = data['n'], data['N'], data['G_FUV'], data['radius']
        dust = data['dust']
        names=['accretion','xray','DG_heat','DG_cool','H2_formation', 
               'FUV','FUV_C','FUV_H2','FUV_CO','FUV_H2O','FUV_OH','FUV_O2',
               'FUV_phel','LyAlpha','H2','HIIrecomb','CO','CO_rot',
               'CO_rovib','H2O_rot','H2O_vib','O_FS','O_FL','dust_gas']
        therm_dtype = dict(names=names, formats=[float]*len(names))
        therm = np.ndarray(len(data['N_H']), dtype=therm_dtype).view(np.recarray)

        # Used for H2 formation heating

        r3 = lambda H, H2, T, T_d, a_g  :  k3(H, H2, T, T_d, a_g) 
        def k3(H, H2, T_g, T_d, a_g, S=0.2) : 
            eta = 0.0        
            if T_d < 25 : eta = 1.0 
            if (T_d >= 25 and T_d < 80)  : eta = 0.6 
            if (T_d >= 80 and T_d < 900) : eta = 0.3 
            if T_d >= 900                : eta = 0.0
            return 4.32e-19 * T_g**0.5 * H * (H + 2.*H2) * eta * S / a_g

        def Gamma_FUV(G, n, E):
            """FUV heating rate in erg/s for a single species, where
            G is photorate [s-1], n is density [cm-3], and E is energy 
            available for heating [ev] per dissociation.
            """
            eV2erg = 1.6021765e-12 # [eV] -> [erg]
            Gamma = G*n*E*eV2erg 
            return Gamma

        for z in range(len(data['N_H'])):
            t = -1 ; nH = data['nH'][z] ; T_g = n['T'][z,t]
            therm.accretion[z] = heating.accretion(nH, n['H2'][z,t], 
                                                   n['He'][z,t], 
                                                   n['e-'][z,t], 
                                                   T_g, 
                                                   R=radius, 
                                                   alpha_H=data['alpha_h'])
            therm.xray[z] = heating.xray(T_g, n['e-'][z,t], n['H'][z,t], n['H2'][z,t], 
                                               n['CO'][z,t], n['H2O'][z,t], n['O'][z,t],
                                               data['zeta']['total'][z])
            therm.H2_formation[z] = heating.H2_formation(
                                      rate=r3(n['H'][z,t],n['H2'][z,t], T_g,n['T_d'][z,t],dust['a_g']))

            therm.FUV[z] = heating.FUV(G_FUV[z], n['C'][z,t], n['H2'][z,t], 
                                       n['CO'][z,t], n['H2O'][z,t], n['OH'][z,t], n['O2'][z,t] )

            therm.FUV_C[z]   = Gamma_FUV(G_FUV['C'][z],   n['C'][z,t],   1.1 )
            therm.FUV_H2[z]  = Gamma_FUV(G_FUV['H2'][z],  n['H2'][z,t],  13.9)
            therm.FUV_CO[z]  = Gamma_FUV(G_FUV['CO'][z],  n['CO'][z,t],  10.7) 
            therm.FUV_H2O[z] = Gamma_FUV(G_FUV['H2O'][z], n['H2O'][z,t], 3.1)
            therm.FUV_OH[z]  = Gamma_FUV(G_FUV['OH'][z],  n['OH'][z,t],  7.7)
            therm.FUV_O2[z]  = Gamma_FUV(G_FUV['O2'][z],  n['O2'][z,t],  6.3)

            therm.FUV_phel[z] = heating.FUV_phel(dust['Y'], dust['E_phel'], data['F_FUV'][z], 
                                                 dust['Q_abs'], dust['S_d'], nH)  
            therm.DG_heat[z] = heating.dust_gas(n['T_d'][z,t], T_g, nH, 
                                                n['H'][z,t], n['H2'][z,t], dust['a_g'])            

            therm.DG_cool[z] = cooling.dust_gas(T_g, nH, n['H'][z,t], n['H2'][z,t], dust['a_g'])
            therm.dust_gas[z] = therm.DG_cool[z] - therm.DG_heat[z]
            therm.LyAlpha[z] = cooling.LymanAlpha(T_g, n['H'][z,t], N['H'][z])
            therm.H2[z] = cooling.H2(T_g, n['H2'][z,t])
            therm.HIIrecomb[z] = cooling.HIIrecomb(T_g, n['e-'][z,t], n['H+'][z,t])
            therm.CO_rot[z], therm.CO_rovib[z] = cooling.CO(T_g, n['CO'][z,t], 
                                N['CO'][z], n['H'][z,t], n['H2'][z,t], nH)
            therm.CO[z] = therm.CO_rot[z] + therm.CO_rovib[z]
            therm.H2O_rot[z], therm.H2O_vib[z] = cooling.H2O(T_g, n['H2O'][z,t], n['H2'][z,t], 
                                                   N['H2O'][z])
            therm.O_FS[z], therm.O_FL[z] = cooling.O(T_g, n['O'][z,t], N['O'][z], n['e-'][z,t],
                                               n['H'][z,t], n['H2'][z,t], nH)
        data.update({'therm':therm})

        return data

    # Identify files and run the calculation

    reactionfile   = dirname+'/reactions.rxn'
    kineticsmodule = dirname+'/kinetics.py'
    reactionmodule = dirname+'/reactions.py'

    from . import kinetics

    # Store version of code used for calculation
    copy2(__file__, output_file + '.py')
    copy2(reactionfile, output_file + '.rxn')
    copy2(reactionmodule, output_file + '.reactions.py')
    copy2(kineticsmodule, output_file + '.kinetics.py')
    copy2(dirname+'/heating.py', output_file + '.heating.py')
    copy2(dirname+'/cooling.py', output_file + '.cooling.py')

    vdata, status = vertical_structure(r, disk_structure, xray_rates, zlimits=(zmin, zmax), t_0=t_0, t_max=t_max)
    logger.info(status)
    if status != 'Failed.':
        logger.info("Appending thermal rates to dataset...")
        output_data = append_therm(vdata)
        np.save(os.path.join(path_out, output_file), output_data)
        logger.info("Saving final output data to {:s}".format(path_out + output_file))
        logger.info("Calculation complete. Creating zip file archive...")

        archive_list = [output_file + '.log', 
                        output_file + '.debug.log',
                        output_file + '.npy',
                        output_file + '.py', 
                        output_file + '.rxn',
                        output_file + '.reactions.py',
                        output_file + '.kinetics.py',
                        output_file + '.heating.py',
                        output_file + '.cooling.py']

        zf = zipfile.ZipFile(os.path.join(path_out, f'{output_file}.zip'), mode='w')
        for filename in archive_list :
            zf.write(filename, compress_type=zipfile.ZIP_DEFLATED)
        zf.close()


@click.command(context_settings={'show_default': True})
@click.option("--config-file", type=str, help="Path to configuration .ini file")
def cli(config_file: str) -> None:

    default_config_path = os.path.join(os.path.dirname(__file__), 'defaults.toml')
    with open(default_config_path, mode="rb") as fp:
        config = tomllib.load(fp)
    if config_file:
        with open(config_file, mode="rb") as fp:
            config.update(tomllib.load(fp))

    vstruct(config)


if __name__ == "__main__":
    cli()



