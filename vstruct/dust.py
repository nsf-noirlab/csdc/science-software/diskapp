from .constants import mH

def set_dust_properties(a_g, Q_abs, Q_ext, rho_bar, dust_gas_ratio, Y, E_phel):
        """Set the dust surface area, scattering, and photo-electric properties.
        """

        S_d = 0.75 * dust_gas_ratio / ((rho_bar / (1.35 * mH)) * a_g * 1e-4)  
        sigma = Q_ext * S_d

        dust = {'a_g':a_g,
                'S_d':S_d,
                'sigma':sigma,
                'Q_abs':Q_abs, 
                'Q_ext':Q_ext, 
                'rho_bar':rho_bar,
                'dust_gas_ratio':dust_gas_ratio, 
                'Y':Y, 
                'E_phel':E_phel,
                'properties':"""\nDust grain properties:
        geometric mean grain size . a_g = sqrt(a1*a2) ={a_g:9.3f} [um]
        grain surface area per H nucleus ........ S_d ={S_d:9.2e} [cm2]
        grain absorption efficiency ........... Q_abs ={Q_abs:9.2f}      
        grain extinction efficiency ........... Q_ext ={Q_ext:9.2f}      
        grain extinction cross section ........ sigma ={sigma:9.2e} [cm2]
        dust/gas mass density ratio ..... rho_d/rho_g ={dust_gas_ratio:9.3f}
        bulk grain density .................. rho_bar ={rho_bar:9.3f} [g/cm3]
        photoelectric yield ....................... Y ={Y:9.3f}
        photo-electron energy (1100-1550A) ... E_phel ={E_phel:9.3f} [eV] 
        """}

        return dust
