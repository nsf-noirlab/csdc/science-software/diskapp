"""
   Heating mechanisms in protoplanetary disk atmospheres. 
   
   References -

   + Initial description: 
     Glassgold, Najita, & Igea, 2004 (GNI04).
     
   + X-rays heating:
     Glassgold, Galli, & Padovani, 2012.

   + FUV heating (photochemical & photoelectic):
     
   All methods return thermal heating rates, Gamma, in [erg/cm3/s].

                                                    mate@berkeley.edu
"""

__version__ = '0.9.4.4'

from numpy import sqrt, pi, exp, arange, zeros, ones, where, log10

eV2erg = 1.6021765e-12 # [eV] -> [erg] unit conversion
ALPHA_H = 0.50

def accretion(nH, n_H2, n_He, n_e, T_g, R=1.0, M=0.5, alpha_H=ALPHA_H):
    """
    Viscous accretion heating driven by MRI as described in
    Glassgold, Najita, & Igea, 2004, Equation 14

    nH      - number denisty of total hydrogen nuclei, [cm-3]
    n_H2    - number denisty of molecular hydrogen, [cm-3]
    n_He    - number denisty of helium, [cm-3]
    n_e     - electron number density, [cm-3]
    T_g     - gas temperature, [K]
    R       - radial cylindrical coordinate, [AU]
    alpha_H - alpha-disk parameter relating viscous heating
              to accretion rate.
    """
    
    x_H2 = n_H2/nH
    x_He = n_He/nH
    x_e  = n_e/nH
    x_HA = 6.4e-4     # from initial abundances

    Gamma = 6.18e-23*alpha_H*nH * (T_g*(1-x_H2+x_He+x_e+x_HA) + 30.3) * \
            M**(0.5) * R**(-1.5) # [erg/cm3/s]

    return Gamma

def xray(T_g, n_e, n_H, n_H2, n_CO, n_H2O, n_O, zeta, W=37.0):
    """
    X-ray heating from scattering, dissociation, rotational &
    vibrational excitation, and chemical heating described in
    Glassgold, Galli, & Padovani, 2012.

    n_e  - electron number density, [cm-3]
    n_H  - volumetric denisty of atomic hydrogen, [cm-3]
    n_H2 - volumetric denisty of molecular hydrogen, [cm-3]
    ...
    zeta - total ionization rate, [s-1]
    W    - average energy per ion pair, [eV]    
    """

    kB     = 1.3806503e-16   # Boltzmann constant, [erg/K]
    eV2erg = 1.6021765e-12   # conversion factor
    m      = 2.38e-24        # average mass of particle, [g]
    
    nH    = n_H + 2.0*n_H2

    x_H   = n_H/nH
    x_e   = n_e/nH
    x_H2  = n_H2/nH
    x_CO  = n_CO/nH
    x_H2O = n_H2O/nH
    x_O   = n_O/nH

    # Elastic scattering and rotational excitation

    eta = lambda x, n, a, C : 1 - ( (1-n)/(1 + C*x**a) )

    eta_H  = eta(x_e, 0.117, 0.678, 7.95)  
    eta_H2 = eta(x_e, 0.055, 0.366, 2.17)

    Q_elrot = W * (x_H*eta_H + x_H2*eta_H2)/(x_H + x_H2)

    # Dissociation heating

    D,a,C = 2.2, 0.574, 22.0
    
    Q_diss = (x_H2/(x_H+x_H2)) * (D/(1+C*x_e**a))

    # Vibrational heating

    eps = lambda x, W, C, a : W*(1+C*x**a)

    eps1 = eps(x_e, 7.81, 23500., 0.955)
    eps2 = eps(x_e, 109., 10700., 0.907)

    Q_vib = (x_H2/(x_H+x_H2)) * 36.8 * (0.512/eps1 + 1.032/eps2) # Eq (13)

    epsB = eps(x_e, 117, 7.09, 0.779)
    epsC = eps(x_e, 132, 6.88, 0.802)

    Q_vib_BC = (x_H2/(x_H+x_H2)) * 36.8 * (3.50/epsB + 4.50/epsC)  # [ev]

    # Chemical heating    
    
    fH2p = 0.877                              # Table 1, 1keV/W
    qe = 7.6                                  # q_e(H2+), [ev]
 
    q1,q2,q3,q_He = 11.1, 7.8, 5.7, 15.6
    k1,k2,k3      = 1.6e-9, 5.3e-9, 0.8e-9

    beta_prime = 4.5e-6 * T_g**(-0.65) # [cm^3/s]

    rate = x_CO*k1 + x_H2O*k2 + x_O*k3 + beta_prime*x_e

    B1 = (1/rate)*k1*x_CO
    B2 = (1/rate)*k2*x_H2O
    B3 = (1/rate)*k3*x_O

    Be = (1/rate)*beta_prime*x_e

    if x_H2 > 0:
        pH3p = x_H2/(x_H2+60.*(T_g**(-0.40))*x_e)  # Eqn (17)
        Q_chem_H2 = (x_H2/(x_H+x_H2)) * fH2p * pH3p * \
                    (B1*q1 + B2*q2 + B3*q3 + Be*qe)
    else:
        Q_chem_H2 = 0

    Q_chem_H  = (x_H2/(x_H+x_H2)) * 0.040 * \
                 (B1*(q1-1.83) + B2*(q2-1.83) + B3*(q3-1.83))

    Q_chem_He = (x_H2/(x_H+x_H2)) * 0.080 * q_He

    Q_chem = Q_chem_H2 + Q_chem_H + Q_chem_He

    # Total X-ray heating rate

    Q = Q_elrot + Q_diss + Q_vib + + Q_vib_BC + Q_chem

    Gamma = zeta * nH * Q * eV2erg # [erg/cm3/s]

    return Gamma

def dust_gas(T_d, T_g, nH, n_H, n_H2, a_g, dust_to_gas_ratio=0.01):
    """
    Dust-gas cooling via collisional thermalization. Starting from 
    Equation (5) from Glassgold, Najita, & Igea, 2004, then
    simplifying, substituting, and only using the term that depends
    on the gas temperature, T_g. Here we set the convention that this 
    term is described as 'cooling', which applies when T_g > T_d, 
    however, the net effect is heating when T_g < T_d.

    T_g  - gas temperature [K].
    nH   - number density of hydrogen nuclei, n(H) + 2*n(H2) [cm-3]
    n_H  - number density of atomic hydrogen, n(H) [cm-3]
    n_H2 - number density of molecular hydrogen, n(H2) [cm-3]
    a_g  - geometric mean of the minimum and maximum grain size [um]
    
    Accomodation coefficients for molecular and atomic hydrogen 
    (AH2 and AH, respectively) fit to Burke & Hollenbach, 1983 
    for silicates at T_d=1000K.
    """

    x_H  = n_H/nH
    x_H2 = n_H2/nH
    
    AH  = lambda T_g : 0.0934 + 0.3393*exp(-((log10(T_g)-0.9789)/1.7966)**2)
    AH2 = lambda T_g : 0.0518 + 0.1232*exp(-((log10(T_g)-1.1806)/1.9856)**2)
    A_H = x_H*AH(T_g) + 2.*x_H2*AH2(T_g)

    Gamma = 2.38e-33*(dust_to_gas_ratio/0.01)*(0.05/a_g)*(A_H/0.5)*T_g**0.5*nH**2 * T_d

    return Gamma

def H2_formation(rate=0, zH=0.33) :
    """
    HEATING DUE TO H2 FORMATION ON GRAINS. 
    The assumed mechanism is the production of ~30% of the H2 in 
    vibrationally excited states 
    (lab work from Lemaire et al., 2010). The H2 molecules collisionally 
    de-excite and deposit ~1.5eV of energy into translational kinetic 
    energy. 

    rate - volumetric rate of H2 molecule formation, R [molecules cm-3 s-1]
    zH   - fraction of binding energy (4.48eV) converted to translational kinetic energy.

    Eb = 7.178e-12 [erg] is th H2 binding energy per molecule (4.48eV)

    heating rate, Gamma in [erg/cm3/s].
    """
    return 7.178e-12*zH*rate

def FUV(G_FUV, n_C, n_H2, n_CO, n_H2O, n_OH, n_O2) :
    """
    Photochemical heating due to both direct heating
    from translational energy and also the maximum subsequent 
    chemical reactions in dense regions, except for C which 
    is not co-located with molecular species.

    G_FUV - FUV photodissociaton rates [s-1]
    n_X   - local volumetric density of species X [cm-3] 
    Q     - total energy available for heating per dissociation [eV]
            from Table 7 ("Total Very Dense") of GN15 (ms052715), with
            O2 from appendix of ANG15.
    """

    Q = {'C':  8.0*eV2erg,
         'H2':12.5*eV2erg,
         'CO': 8.7*eV2erg,
         'H2O':2.1*eV2erg,
         'OH': 5.5*eV2erg,
         'O2': 6.3*eV2erg,
         }

    Q_Lya = {'H2O':1.6*eV2erg,
             'OH': 6.4*eV2erg,
             }

    Gamma_C   = G_FUV['C']  *n_C  *Q['C']  
    Gamma_H2  = G_FUV['H2'] *n_H2 *Q['H2'] 
    Gamma_CO  = G_FUV['CO'] *n_CO *Q['CO'] 
    Gamma_H2O = (G_FUV['H2O']*Q['H2O']+G_FUV['H2O_Lya']*Q_Lya['H2O'])*n_H2O
    Gamma_OH  = (G_FUV['OH']*Q['OH']+G_FUV['OH_Lya']*Q_Lya['OH'])*n_OH 
    Gamma_O2  = G_FUV['O2'] *n_O2 *Q['O2'] 

    Gamma = Gamma_C + Gamma_H2 + Gamma_CO + Gamma_H2O + Gamma_OH + Gamma_O2

    try:
        return float(Gamma)
    except:
        return Gamma

def FUV_phel(Y, E_phel, F_FUV, Q_abs, S_d, nH):
    """ Grain photoelectric heating. 

    FUV heating also occurs by the photoelectric effect on grains and 
    PAHs. The underlying physics is very complicated because it depends
    on the poorly known properties of the grains, such as the size and
    charge distributions. In addition to the grain surface area per 
    H nucleus, S_d and the FUV numer flux, some of the relevant properties are
    the absorption coefficient Qabs, the probability for ejecting an 
    electron Y, and the energy of the photoelectron energy, E_phel. All
    of these quantities depend on photon energy. In view of the 
    uncertainties in the physical and chemical properties of the grains
    and small particles, we do not carry out a calculation band by band, 
    as for photodissociation, but adopt average values for each of the
    quantities that enter into the heating rate per unit volume.
    """
    return float(Y * E_phel * F_FUV * Q_abs * S_d * nH * eV2erg)



    
