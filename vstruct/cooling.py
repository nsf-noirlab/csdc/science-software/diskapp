#!/usr/bin/env python
"""
  Cooling mechanisms in protoplanetary disk atmospheres. 
  Each of the following functions return the thermal cooling rate, 
  Lambda, in units of [erg/cm3/s].

   + version 0.9.2 - consistent dv throughout and cleaned up diagnostics.
   + version 0.9.1 - corrected CO rovib cooling rate for Ahat mixup.

  A note on line widths: sigma is variance of velocity width
                            dv = sqrt(2pi*sigma**2)    (line width, FWHM)
                             b = sqrt(2)*sigma         (thermal broadenning)

                                                       mate@berkeley.edu
"""
__version__ = '0.9.2'

from numpy import sqrt, pi, exp, arange, zeros, ones, where, array, clip, log10
from scipy.interpolate import interp1d, interp2d, bisplrep, bisplev

kB  = 1.3806503e-16           # Boltzmann constant, [erg/K]    
e   = 4.8032068e-10           # electron charge, [esu]
c   = 2.9979246e+10           # speed of light, [cm/s]
m_e = 9.1093897e-28           # electron mass, [g] 
mH  = 1.6733e-24              # mass of hydrogen, [g]

sigma_turb_sq  = 1.0e10       # turbulent line variance [cm/s] squared 

def dust_gas(T_g, nH, n_H, n_H2, a_g, dust_to_gas_ratio=0.01) :
    """
    Dust-gas cooling [erg/cm3/s]:

    T_g  - gas temperature [K].
    nH   - number density of hydrogen nuclei, n(H) + 2*n(H2) [cm-3]
    n_H  - number density of atomic hydrogen, n(H) [cm-3]
    n_H2 - number density of molecular hydrogen, n(H2) [cm-3]
    a_g  - geometric mean of the minimum and maximum grain size [um]

    Output: Cooling rate.

    Using Equation (5) from Glassgold, Najita, & Igea, 2004
    simplifying, substituting, and only using the term that depends
    on the gas temperature, T_g. We set the convention that this term
    is described as 'cooling', which applies when T_g > T_d, however,
    the net effect is heating when T_g < T_d.

    Accomodation coefficients for molecular and atomic hydrogen 
    (AH2 and AH, respectively) fit to Burke & Hollenbach, 1983 
    for silicates at T_d=1000K.
    """
    x_H  = n_H/nH
    x_H2 = n_H2/nH
    
    AH  = lambda T_g : 0.0934 + 0.3393*exp(-((log10(T_g)-0.9789)/1.7966)**2)
    AH2 = lambda T_g : 0.0518 + 0.1232*exp(-((log10(T_g)-1.1806)/1.9856)**2)
    A_H = x_H*AH(T_g) + 2*x_H2*AH2(T_g)

    Lambda = 2.38e-33*(dust_to_gas_ratio/0.01)*(0.05/a_g)*(A_H/0.5)*T_g**0.5*nH**2 * T_g

    return Lambda                 

def LymanAlpha(T_g, n_H, N_H) :
    """
    LYMAN ALPHA COOLING

    T_g - gas temperature [K].
    n_H - atomic hydrogen density.
    N_H - total hydrogen vertical column density.

    Using Appending C from Glassgold, Najita, & Igea, 2004 (GNI04)
    """
    lambda_21 = 1.217e-5          # transition wavelength, [cm]
    f_12      = 0.4162            # tranistion probability
    E_21      = 1.635e-11         # transition energy
    A_21      = 4.70e8            # Einstein A coefficient
                                  # 10.2eV * 11604K/eV = Energy level (n=2) in K

    n2  = 4.0 * exp( -118418.0/T_g) * n_H # population in n=2 level (thermal)

    sigma_therm_sq = kB*T_g / mH
    dv = sqrt( 2*pi*(sigma_therm_sq + sigma_turb_sq) )

    tau_21  = N_H * f_12 * (pi*e**2)/(m_e*c) * lambda_21 / dv
    beta_21 = (1. - exp(-3*tau_21))/(3*tau_21)
    Lambda  = beta_21 * A_21 * E_21 * n2

    return Lambda

def H2(T_g, n_H2) :
    """
    H2 ROT. & ROVIB. 
    Assuming optically thin H2 cooling, and using a
    fit to the Le Bourlot et al. cooling curve for their maximum density
    (nH = 1e8 cm-3) and minimum H/H2 ratio (1e-2).
    """
    return 2.70e-34 * T_g**4.25 * n_H2 

def HIIrecomb(T_g, n_e, n_HII) :
    """
    HYDROGEN ION (H+ or HII) RECOMBINATION COOLING.

    Recombination cross-section:     4e-13 [cm3/s]
    Recombination rate coefficient:  Table 5.2 in Spitzer
    """
    return 4.18e-26 * (T_g**0.199) * n_e * n_HII

def CO(T_g, n_CO, N_CO, n_H, n_H2, nH, Jmax=100) :
    """
    CO ROTATION AND RO-VIBRATIONAL COOLING
    """

    x_H  = n_H / nH
    x_H2 = n_H2/ nH

    sigma_therm_sq = kB*T_g/(28*mH)
    dv = sqrt( 2*pi*(sigma_therm_sq + sigma_turb_sq) )

    # Rotational cooling
    J   = arange(0,Jmax,1)
    tau = ones(Jmax)
    
    A10 = 7.17e-8           # frequency of J=1-0 transition in CO, [s-1] 
    B   = 2.766             # ( B/kB = 2.766 [K] ) 
    T10 = 2*B               # 
    lambda10 = 2.60 / 10.   # [mm]/10 -> [cm] 
        
    P = (B/T_g) * exp( -J * (J+1) * B/T_g ) # rotational population, P_J     
    A = ( 3*A10*J**4. ) / ( 2*J+1 )         # Einstein A-coefficient, A_{J,J-1} 
    E = 2.*B *(J + 1) * kB                  # Transition energy, E_{J,J-1}
   
    for j in arange(1,Jmax,1) :
        tau[j] = (2*j+1.) * P[j] * (1.-P[j]/P[j-1]) * (N_CO/(8*pi*dv)) * \
                 A[j] * (lambda10/j)**3

    beta = (1 - exp( - 3.*tau))/(3*tau)
    Lambda_rot = n_CO * sum( beta[1::] * A[1::] * P[1::] * E[1::] ) 
  
    # Rovibrational cooling
    A_10 = 37.                    # [s-1], after Eqn 12 in therm11.2
    E_10 = 3084.*kB               # E_{1,0} / kB [erg]
    l_10 = 4.7e-4                 # (1-0) transition wavelenghth, 4.7um, in [cm]
    P0   = 1.0
    f_lu = 5e-6

    tau  = f_lu * (pi*e**2/(m_e*c)) * (l_10/dv) * N_CO
    beta = (1 - exp(-3*tau))/(3*tau)
                                             # Collisional excitation rates 
    k_10_H  = 4.93e-11 * exp(-1500/T_g)      #   Balakrishnan et al., 2002
    k_10_H2 = 4.3e-14 * exp(-68*T_g**(-1/3)) #   Nuefeld & Kaufman, 1993

    k_10    = x_H*k_10_H + x_H2 * k_10_H2   
    A_10hat = A_10 * beta
    n_crhat = (A_10/k_10) * beta

    Lambda_rovib = A_10hat * E_10 * n_CO * 1/(1 + exp(E_10/(kB*T_g))*(1 +(n_crhat/nH)) )

    return Lambda_rot, Lambda_rovib

def H2O( T_g, n_H2O, n_H2, N_H2O) :
    """
    H2O ROTATIONAL & VIBRATIONAL COOLING
    Transcription of Table 2 from Nuefeld & Kaufman, 1993. 
    T    - temperatures, [K]; 
    logN - log vertical column, \tilde N, [cm-2]
    L0   - cooling rate coefficient in low density limit [erg/cm3/s]
    L    - cooling rate parameters, L_LTE
    n    - critical density, n_{1/2}
    a    - parameter, alpha
    Output: Cooling rate
    """
    
    data_T    = array( [100,200,400,1000,2000,4000] )     
    data_logN = array( [10,11,12,13,14,15,16,17,18,19] ) 
    data_L0   = array( [24.35, 23.87, 23.42, 22.88, 22.50, 22.14] ) 
                                                               
    data_L = array([ [14.59, 13.85, 13.16, 12.32, 11.86, 11.64],
                     [14.59, 13.86, 13.16, 12.32, 11.86, 11.64],
                     [14.60, 13.86, 13.16, 12.32, 11.86, 11.64],
                     [14.68, 13.88, 13.17, 12.32, 11.86, 11.64],
                     [14.98, 14.05, 13.25, 12.34, 11.87, 11.65],
                     [15.53, 14.46, 13.53, 12.49, 11.97, 11.72],
                     [16.22, 15.05, 14.02, 12.87, 12.35, 12.06],
                     [17.00, 15.74, 14.63, 13.46, 12.97, 12.66],
                     [17.83, 16.50, 15.32, 14.16, 13.69, 13.36],
                     [18.70, 17.31, 16.07, 14.94, 14.46, 14.13] ])

    data_n = array([ [ 9.00,  9.04,  9.19,  9.50,  9.67,  9.60],  
                     [ 8.99,  9.04,  9.19,  9.50,  9.67,  9.60],
                     [ 8.96,  9.03,  9.19,  9.50,  9.66,  9.59], 
                     [ 8.74,  8.89,  9.11,  9.47,  9.65,  9.59], 
                     [ 8.11,  8.37,  8.73,  9.31,  9.56,  9.53], 
                     [ 7.20,  7.51,  7.95,  8.74,  9.15,  9.20], 
                     [ 6.22,  6.53,  6.99,  7.87,  8.38,  8.50], 
                     [ 5.22,  5.57,  6.03,  6.94,  7.48,  7.64], 
                     [ 4.24,  4.59,  5.09,  6.02,  6.59,  6.78], 
                     [ 3.21,  3.58,  4.10,  5.08,  5.69,  5.89] ])

    data_a = array([ [ 0.43,  0.42,  0.39,  0.36,  0.34,  0.34],
                     [ 0.43,  0.42,  0.39,  0.36,  0.34,  0.34],
                     [ 0.42,  0.41,  0.39,  0.36,  0.34,  0.34],
                     [ 0.41,  0.39,  0.37,  0.35,  0.33,  0.33],
                     [ 0.42,  0.38,  0.34,  0.33,  0.32,  0.32],
                     [ 0.45,  0.38,  0.34,  0.32,  0.30,  0.30],
                     [ 0.47,  0.40,  0.35,  0.32,  0.29,  0.30],
                     [ 0.50,  0.42,  0.36,  0.32,  0.28,  0.29],
                     [ 0.52,  0.44,  0.37,  0.31,  0.27,  0.28],
                     [ 0.53,  0.45,  0.39,  0.31,  0.27,  0.27] ])

    sigma_therm_sq = kB*T_g/(18*mH)
    dv = sqrt( 2*pi*(sigma_therm_sq + sigma_turb_sq) )

    N_tilde = N_H2O / dv

    T = clip( T_g, min(data_T), max(data_T) ) 
    n = clip( log10( N_tilde ), min(data_logN), max(data_logN) )

    f_L0 = interp1d( data_T, data_L0, kind='cubic' )              
    L0   = 10**( -1 * f_L0(T) )

    # Replicate T and logN arrays for spline fitting
    x = data_T.repeat( len(data_logN) ).reshape( len(data_T), len(data_logN) ).T
    y = data_logN.repeat( len(data_T) ).reshape( len(data_logN), len(data_T) )

    L_tck = bisplrep( x, y, data_L, kx=1, ky=1)
    L_LTE = bisplev( T, n, L_tck )

    n_tck = bisplrep( x, y, data_n, kx=1, ky=1)
    n_cr  = bisplev( T, n, n_tck )

    a_tck = bisplrep( x, y, data_a, kx=1, ky=1)
    alpha = bisplev( T, n, a_tck )

    L = 1 / ( (1/L0) + (n_H2/L_LTE) + ( (1/L0)*(n_H2/n_cr)**alpha * (1 - (n_cr*L0/L_LTE)) ) )

    Lambda_rot = L * n_H2 * n_H2O
    
    # vibrational cooling:

    f_L0 = lambda T : 1.03e-26 * T * exp(-47.5/T**(1/3.)) * exp(-2325./T)
    L0   = f_L0( T )
    
    data_logN = array( [13,14,15,16,17,18,19,20] ) 

    data_L = array([ [10.98, 11.05, 11.07, 10.88, 10.67, 10.67],
                     [10.99, 11.05, 11.07, 10.88, 10.67, 10.67],
                     [11.06, 11.09, 11.09, 10.88, 10.67, 10.67],
                     [11.36, 11.31, 11.23, 10.93, 10.69, 10.68],
                     [11.91, 11.82, 11.65, 11.15, 10.79, 10.72],
                     [12.59, 12.50, 12.28, 11.61, 11.11, 10.90],
                     [13.29, 13.20, 12.99, 12.25, 11.65, 11.30],
                     [13.90, 13.90, 13.76, 13.01, 12.34, 11.88] ])
                      
    x = data_T.repeat( len(data_logN) ).reshape( len(data_T), len(data_logN) ).T
    y = data_logN.repeat( len(data_T) ).reshape( len(data_logN), len(data_T) )

    L_tck = bisplrep( x, y, data_L, kx=1, ky=1)
    L_LTE = 10**( -1 * bisplev( T, n, L_tck ) ) / exp( 2325/T )

    L = 1 / ( (1/L0) + (n_H2/L_LTE) )

    Lambda_vib = L * n_H2 * n_H2O

    return Lambda_rot, Lambda_vib


def O( T_g, n_O, N_O, n_e, n_H, n_H2, nH):
    """
    ATOMIC OXYGEN COOLING from fine structure (FS) and
    forbidden line (FL) cooling.
    """
    # OI Fine structure 

    T_21 = 227.7           
    T_32 = 98.86           
    
    E_21 = T_21*kB         # fine structure energy [ergs]
    E_32 = T_32*kB        

    A_21 = 8.91e-05        # Einstein A-values (u-l: 2-1) [s-1]
    A_32 = 1.75e-05        #                   (u-l: 3-2) [s-1]

    lambda_21 = 63.185e-4  # transition wavelength, 63um [cm]
    lambda_32 = 145.53e-4  #                        44um [cm]

    Z   = 5. + 3*exp( -T_21/T_g ) + exp( -T_32/T_g ) # Partition function

    P_1  = 5./Z                      # Thermal equilibrium population (level 1)
    P_2  = (3/Z) * exp( -T_21/T_g )
    P_3  = (1/Z) * exp( -T_32/T_g )
    
    sigma_therm_sq = kB*T_g/(16*mH)
    dv = sqrt( 2*pi*(sigma_therm_sq + sigma_turb_sq) )

    tau_21 = (1/(8*pi*dv))*(3/5.) * P_1 * N_O * lambda_21**2 * (1-exp( -T_21/T_g ))
    tau_32 = (1/(8*pi*dv))*(1/3.) * P_2 * N_O * lambda_32**2 * (1-exp( -T_32/T_g ))

    beta_21 = (1 - exp(-3*tau_21))/(3*tau_21)
    beta_32 = (1 - exp(-3*tau_32))/(3*tau_32)

    Lambda_FS = n_O * (P_2*A_21*beta_21*E_21 + P_3*A_32*beta_32*E_32)

    # Forbidden line

    A_42 = 1.82e-03      
    A_41 = 5.65e-03      

    E_42 = 22605*kB
    E_41 = 22840*kB

    T_4l = 22722        # average of 4-1 and 4-2 transition energies [K]
    A_4l = 7.47e-3      # sum of 4-1 and 4-2 Einstein A-coefficients [cm-3]

    E_51 = 48410*kB
    E_52 = 48630*kB
    E_5  = (E_51+E_52)/2  # average of 5-1 and 5-2 transition energies

    k_bar_4l = (1.73e-13*T_g**0.224*n_H + 1.36e-10*T_g**0.465*n_e + 1e-12*n_H2)/nH
    n4_cr = A_4l/k_bar_4l  
    P_4   = (1 + (9/5.)*exp( T_4l/T_g) * (1 + n4_cr/nH) )**(-1)
      
    Lambda_FL_4 = n_O * P_4 * (A_42*E_42 + A_41*E_41)
  
    return Lambda_FS, Lambda_FL_4
