LOG_HEADER = """

    Dust recombination (tol=1e-10):
        + Additional molecules recombining
        + Dust-mediated recombination of charged atoms and the
          most abundant charged molecules on grains.
          Assumes grains are multiply negatively charged 
        + Test if tolerances changes charge imbalance
        + Implementation of expanded sulfur chemistry 
          including sulfur photochemistry.
        + 14 New species including:
           S2, HCS, H2CS, S2+, HS2+, CS2, 
           OCS, HSO+, HSO2+, OCS+, HOCS+, 
           C2S, H2CS+, HCS2+
        + This version of the model builds directly 
          on model version 0.9.4.98, which included
          Lya photodissocion of ammonia, and is presented 
          in Najita & Adamkovics, 2017. Please follow notes in 
          that reference for prior modifications.

    NOTES:
        + This version is currently for testing the chemistry 
          revisions and implementation.
        + This version runs with Python3

    REVISIONS BEYOND ANG16,NA17:
        + Sulfur chemistry
        + Dust-mediated recombination with e-

    """
